import { combineReducers, configureStore } from '@reduxjs/toolkit';
import { Reducer } from 'redux';
import { persistStore } from 'redux-persist';
import { TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux';
import { RESET_STATE_ACTION_TYPE } from '../actions/resetState';
import MarketPlaceReducer from "../../features/Market";
import MerchantProfileReducer from "../../features/Merchant";
import AppStateReducer from "../../features/App";
import { auth_service } from '../../service/auth.service';
import { authReducer} from "../../features/Authentication";
import { unauthenticatedMiddleware } from '../../middlewares/authenticationMiddleware';
import marketServiceApi from '../../service/varroe/services/api';
import userServiceApi from '../../service/varroe/user/api';

const reducers = {
  applicationState: AppStateReducer,
  auth: authReducer,
  marketplace: MarketPlaceReducer,
  merchantProfile: MerchantProfileReducer,
  [userServiceApi.reducerPath]: userServiceApi.reducer,
  [marketServiceApi.reducerPath]: marketServiceApi.reducer,
  [auth_service.reducerPath]: auth_service.reducer,
};

const combinedReducer = combineReducers<typeof reducers>(reducers);
export const rootReducer: Reducer<RootState> = (
  state,
  action
) => {
  if (action.type === RESET_STATE_ACTION_TYPE) {
    state = {} as RootState;
  }

  return combinedReducer(state, action);
};

export const store = configureStore({
  reducer: rootReducer,
  middleware: (getDefaultMiddleware) => getDefaultMiddleware({
    serializableCheck: false,
  }).concat([
    auth_service.middleware,
    userServiceApi.middleware,
    marketServiceApi.middleware,
    unauthenticatedMiddleware,
  ]),
});

export const persistor = persistStore(store);


// setupListeners(store.dispatch);


export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof combinedReducer>;
export const useAppDispatch = () => useDispatch<AppDispatch>();
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;
// export type AppThunk<ReturnType = void> = ThunkAction<
//   ReturnType,
//   RootState,
//   unknown,
//   Action<string>
// >;