import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import storage from 'redux-persist/lib/storage';
import { persistReducer } from 'redux-persist';
import { RootState } from '../../app/store';
import {
  MarketPlaceBusinessI,
  BusinessServicesI
} from "../../interfaces/slices";


interface MerchantProfileState {
  activeBusiness: MarketPlaceBusinessI | object;
  activeService: BusinessServicesI | object;
}
const initialState: MerchantProfileState = {
  activeBusiness: {},
  activeService: {}
}

const reducerFunctions = {
  setActiveBusiness(state: MerchantProfileState, action: PayloadAction<MarketPlaceBusinessI>) {
    localStorage.setItem('activeBusiness', JSON.stringify(action.payload));
    return {
      ...state,
      activeBusiness: action.payload,
    }
  },
  setActiveService(state: MerchantProfileState, action: PayloadAction<BusinessServicesI>) {
    localStorage.setItem('activeService', JSON.stringify(action.payload));
    return {
      ...state,
      activeService: action.payload
    }
  }

}


const merchantProfileSlice = createSlice({
  name: "merchantProfile",
  initialState,
  reducers: reducerFunctions,
});
export const {
  setActiveBusiness,
  setActiveService
} = merchantProfileSlice.actions;
const merchantProfileReducer = persistReducer({
  key: 'business',
  storage: storage,
  whitelist: ['activeBusiness', 'activeService'],
}, merchantProfileSlice.reducer);

export default merchantProfileReducer;
export const selectBusiness = (state: RootState) => state.merchantProfile.activeBusiness;
export const selectService = (state: RootState) => state.merchantProfile.activeService;