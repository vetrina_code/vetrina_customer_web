import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../app/store';

interface AppStateSliceI {
  isLoading?: boolean;
  isError?: boolean;
  isSuccess?: boolean;
  error?: string;
  success?: string;
}

const initialState: AppStateSliceI = {
  isLoading: false,
  isError: false,
  isSuccess: false,
  error: '',
  success: '',
}

const reducerFunctions = {
  setAppState(state: AppStateSliceI, action: PayloadAction<AppStateSliceI>) {
    return {
      ...state,
      ...action.payload,
    }
  },
  resetAppState() {
    return {
      ...initialState,
    }
  }
}


const appStateSlice = createSlice({
  name: "application_state",
  initialState,
  reducers: reducerFunctions,
});

export const { setAppState, resetAppState } = appStateSlice.actions;
export const selectAppState = (state: RootState) => state.applicationState;

export default appStateSlice.reducer;