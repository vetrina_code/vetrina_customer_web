import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../app/store';
import { SelectedServiceOptions } from '../../interfaces/designs';
import { MarketPlaceBusinessI } from '../../interfaces/slices';

type StackItem = {
  business: MarketPlaceBusinessI,
  services: SelectedServiceOptions[]
}
interface StackState {
  stack: Array<StackItem>
}
const initialState: StackState = {
  stack: []
}

const reducerFunctions = {
  addServiceToStack(state: StackState, action: PayloadAction<StackItem>) {
    const item = action.payload
    const existingItem = state.stack.find((existing_item) => existing_item.business._id === item.business._id);
      if (existingItem) {
        return {
          ...state,
          stack: state.stack.map((existing_item) => {
            if (existing_item.business._id === existingItem.business._id) {
              return item;
            } else {
              return existing_item;
            }
          }),
        };
      } else {
        return {
          ...state,
          stack: [...state.stack, item],
        };
      }
  },
  removeServiceFromStack(state: StackState, action: PayloadAction<string>){

  }
}


const stackSlice = createSlice({
  name: "stack",
  initialState,
  reducers: reducerFunctions,
});
export const {
  addServiceToStack,
} = stackSlice.actions;

export default stackSlice.reducer;