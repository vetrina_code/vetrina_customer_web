import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../../app/store';
import {
  MarketPlaceBusinessesI,
  MarketPlaceCategoriesI,
  MarketPlaceStateI
} from "../../interfaces/slices";

const initialState: MarketPlaceStateI = {
  location: 'Toronto',
  catgeory: {
    _id: '',
    name: '',
    created_at: '',
    updated_at: '',
    icon_url: ''
  },
  businesses: []
}

const reducerFunctions = {
  loadAllBusinesses(state: MarketPlaceStateI, action: PayloadAction<MarketPlaceBusinessesI>) {
    return {
      ...state,
      buisnesses:[...state.businesses, ...action.payload],
    }
  },
  setLocation(state: MarketPlaceStateI, action: PayloadAction<string>) {
    return {
      ...state,
      location: action.payload,
    }
  },
  setPageNumber(state: MarketPlaceStateI, action: PayloadAction<number>) {
    return {
      ...state,
      page: action.payload,
    }
  },
  setBusinesses(state: MarketPlaceStateI, action: PayloadAction<MarketPlaceBusinessesI>) {
    return {
      ...state,
      businesses: action.payload,
    }
  },
  setCategory(state: MarketPlaceStateI, action: PayloadAction<MarketPlaceCategoriesI>) {
    return {
      ...state,
      catgeory: action.payload,
    }
  }
}


const marketPlaceSlice = createSlice({
  name: "marketPlace",
  initialState,
  reducers: reducerFunctions,
});
export const {
  setLocation,
  loadAllBusinesses,
  setPageNumber,
  setBusinesses,
  setCategory
} = marketPlaceSlice.actions;

export default marketPlaceSlice.reducer;
export const selectBusinesses = (state: RootState) => state.marketplace.businesses;
export const selectLocation = (state: RootState) => state.marketplace.location;
export const selectPageNumber = (state: RootState) => state.marketplace.page;
export const selectCategory = (state: RootState) => state.marketplace.catgeory;