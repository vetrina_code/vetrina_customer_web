import { createSlice, createAsyncThunk, PayloadAction } from '@reduxjs/toolkit';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import { RootState } from '../../app/store';
import {
  AuthPayloadAttributesI, UserT
} from "../../interfaces/slices";

function getUserDetails(): AuthPayloadAttributesI | undefined {
  const user = localStorage.getItem('user');
  const token = localStorage.getItem('token');
  if (user && token) {
    return {
      user: JSON.parse(user) as UserT,
      token,
      isAuthenticated: true,
    };
  }
}
const initialState: AuthPayloadAttributesI = getUserDetails() || {
  token: '',
  user: {
    first_name: '',
    last_name:'',
    email: '',
    id: '',
    account_type: ''
  },
  isAuthenticated: false,
}

export const register = createAsyncThunk('auth.register', async (userData, thunkAPI) => {
  try {
    // return await authService.register(userData);
  } catch (error: any) {
    const message = (error.response && error.response.data && error.response.data.message) || error.message || error.toString();
    return thunkAPI.rejectWithValue(message)
  }
})

const reducerFunctions = {
  logout: () => {
    localStorage.removeItem('user');
    localStorage.removeItem('token');
    return {
      user: {
        first_name: '',
        last_name:'',
        email: '',
        id: '',
        account_type: ''
      },
      token: '',
      isAuthenticated: false,
    }
  },
  setUser: (state: any, action: PayloadAction<AuthPayloadAttributesI>) => {
    const { user, token } = action.payload;
    const isAdmin = user.account_type === 'super_admin';

    localStorage.setItem('user', JSON.stringify(user));
    localStorage.setItem('token', token);
    localStorage.setItem('isAuthenticated', 'true');
    localStorage.setItem('isAdmin', JSON.stringify(isAdmin));
    return {
      ...state,
      ...action.payload,
      user: {
        ...user,
        isAdmin,
      },
      token,
      isAuthenticated: true,
    }
  }
}

export const selectAuth = (state: RootState) => state.auth;

const authenticationSlice = createSlice({
  name: "authentication",
  initialState,
  reducers: reducerFunctions,
});

export const {
  setUser,
  logout
} = authenticationSlice.actions;

// export {
//   register,
//   login,
//   logout,
// }
export const authReducer = persistReducer({
  key: 'auth',
  storage: storage,
  whitelist: ['user', 'token', 'isAuthenticated', 'isAdmin'],
}, authenticationSlice.reducer);