import styled from "@emotion/styled";
import { Grid, Stack } from "@mui/material";

export const BackgroundWrapper = styled(Grid)(() => ({
  width: "100vw",
  height: "100%",
  minHeight: "100vh",
  backgroundImage:
    "linear-gradient(rgba(37, 29, 42, 1), rgba(37, 29, 42, 1)),url(/img/doodleBackground.png)",
  padding: "40px",
}));

export const CenteredStack = styled(Stack)`
  align-items: center;
`;