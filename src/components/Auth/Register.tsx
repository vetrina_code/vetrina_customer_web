import React from "react";
import { useNavigate } from "react-router-dom";
// Types & Interfaces
import { AuthenticationComponentProps } from "../../interfaces/designs";
// Design & Icon Components
import { VisibilityOff, Visibility } from "@mui/icons-material";
import {
  Box,
  Card,
  Button,
  CardContent,
  FormControl,
  IconButton,
  InputAdornment,
} from "@mui/material";
import { CenteredStack } from ".";
import { AppleIcon, GoogleIcon } from "../../assets/svg";
import { FillButton } from "../../ui/Buttons";
import { FillInput } from "../../ui/Form/FilledInput";
import { HelperText } from "../../ui/Text/HelperText";

function Register(props: AuthenticationComponentProps) {
  const navigator = useNavigate();
  const [showPassword, setShowPassword] = React.useState(false);

  return (
    <Card
      sx={{
        width: "400px",
        justifyContent: "center",
        alignItems: "center",
        textAlign: "center",
        fontFamily: "Open Sans !important",
      }}
    >
      <CardContent
        sx={{
          fontWeight: 600,
          fontSize: "16px",
        }}
      >
        <CenteredStack spacing={1}>
          <CenteredStack padding={2} spacing={3}>
            <FillButton
              mycolor={"black"}
              preferredfont="Open Sans"
              startIcon={<AppleIcon />}
            >
              continue with apple
            </FillButton>
            <FillButton
              sx={{
                boxShadow:
                  "0px 0px 2.41919px rgba(0, 0, 0, 0.084), 0px 2.41919px 2.41919px rgba(0, 0, 0, 0.168);",
              }}
              mycolor={"whitesmoke"}
              preferredfont="Open Sans"
              startIcon={<GoogleIcon />}
            >
              continue with google
            </FillButton>
          </CenteredStack>
          <span>OR</span>
          <FormControl
            fullWidth
            size="small"
            sx={{
              alignItems: "flex-start",
            }}
          >
            <HelperText>First Name</HelperText>
            <FillInput
              name="first_name"
              onChange={(event) => props.handleUserDetails(event)}
              margin="dense"
              placeholder="John"
              value={props.userDetails.first_name}
              id="name-input"
              aria-describedby="name"
            />
          </FormControl>
          <FormControl
            fullWidth
            size="small"
            sx={{
              alignItems: "flex-start",
            }}
          >
            <HelperText>Last Name</HelperText>
            <FillInput
              name="last_name"
              onChange={(event) => props.handleUserDetails(event)}
              margin="dense"
              placeholder="Doe"
              value={props.userDetails.last_name}
              id="name-input"
              aria-describedby="name"
            />
          </FormControl>
          <FormControl
            fullWidth
            size="small"
            sx={{
              alignItems: "flex-start",
            }}
          >
            <HelperText>Email</HelperText>
            <FillInput
              name="email"
              onChange={(event) => props.handleUserDetails(event)}
              margin="dense"
              placeholder="johndoe@example.com"
              value={props.userDetails.email}
              id="email-input"
              aria-describedby="email"
            />
          </FormControl>
          <FormControl
            fullWidth
            size="small"
            sx={{
              alignItems: "flex-start",
            }}
          >
            <HelperText>Password</HelperText>
            <FillInput
              name="password"
              type={showPassword ? "text" : "password"}
              onChange={(event) => props.handleUserDetails(event)}
              margin="dense"
              placeholder="hbcy7282hxx"
              value={props.userDetails.password}
              id="password-input"
              aria-describedby="password"
              endAdornment={
                <InputAdornment position="end">
                  <IconButton
                    aria-label="toggle password visibility"
                    onClick={() => setShowPassword(!showPassword)}
                    edge="end"
                  >
                    {showPassword ? <VisibilityOff /> : <Visibility />}
                  </IconButton>
                </InputAdornment>
              }
            />
             <Box sx={{ textAlign: "end", width: "100%", cursor: 'pointer'}} onClick={() => navigator('/reset-password')}>
              <HelperText>Forgot Password ?</HelperText>
            </Box>
          </FormControl>
          <FillButton
            sx={{ color: "white", textTransform: "uppercase !important" }}
            onClick={props.handleAuthenticationRequest}
          >
            REGISTER
          </FillButton>
          <Box sx={{
            textAlign: "center",
            fontSize: "12px !important",
          }}>
            <span>
              Already have an account? 
              <Button
                onClick={() => props.handleRedirect("login")}
                variant="text"
                sx={{
                  fontWeight: 400,
                  fontSize: 'inherit',
                  fontFamily: 'Montserrat',
                  color: '#5c0e90',
                  textTransform: 'none',
                  padding: '0px',
                }}
              >
                {"Login."}
              </Button>
            </span>
          </Box>
        </CenteredStack>
      </CardContent>
    </Card>
  );
}

export default Register;

