import React from 'react'
import { Box, Paper, Grid } from '@mui/material'
import { BusinessServicesI } from '../../interfaces/slices'

function ServiceCard({service} :{service: BusinessServicesI}) {
  const {
    name,
    price,
  } = service;
  
  return (
    <Box sx={{
      width: "250px",
      fontSize: "11px"
      // margin: '0px 20px',
      // maxHeight: "50px",x
    }}>
      <Paper elevation={5} sx={{
        padding: 2
      }}>
        <Grid
          container
          direction="row"
          justifyContent="space-between"
          alignItems="baseline"
        >
          <span>{name}</span>
          <span>${price}</span>
        </Grid>

        <Box sx={{
          width:'70%',
          marginTop: 1.5,
          height: "70px",
          overflow: 'hidden',
        }}>
          <span>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptates eaque est nam voluptas cupiditate cumque expedita provident explicabo labore eligendi blanditiis quo deserunt alias aspernatur facere architecto, maxime, inventore ad?</span>
        </Box>
      </Paper>
    </Box>
  )
}

export default ServiceCard