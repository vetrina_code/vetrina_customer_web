import React from "react";
import { Box, Button, Grid, Rating, Stack } from "@mui/material";
import { VerifiedIcon, EmailIcon, PhoneIcon2, WebIcon } from "../../assets/svg";
import { FillButton } from "../../ui/Buttons";
import { MarketPlaceBusinessI } from "../../interfaces/slices";

function SideDrawer({ business }: { business: MarketPlaceBusinessI }) {
  const {
    business_name,
    business_description,
    category,
    country,
    average_rating
  } = business
  return (
    <Box sx={{
      // maxWidth: '250px',
      width: '100%',
      height: '100%'
    }}>
      <Stack
        spacing={3}
        sx={{
          padding: "20px",
        }}
      >
        <h1>
          <VerifiedIcon />
          <span>{business_name}</span>
        </h1>
        <span>{country}</span>
        <Grid container justifyContent={"space-between"}>
          <Rating size="medium" precision={0.5} defaultValue={average_rating} readOnly />
          <FillButton
            size="small"
            sx={{
              color: "white",
              margin: "0px !important",
            }}
          >
            {category.name}
          </FillButton>
        </Grid>
      </Stack>
      <Stack
        spacing={4}
        sx={{
          padding: "20px",
          backgroundColor: "rgba(141, 108, 159, 1)",
          color: "white",
        }}
      >
        {[
          { icon: EmailIcon, label: "example@example.com" },
          { icon: PhoneIcon2, label: "+234 706 314 9418" },
          { icon: WebIcon, label: "Website" },
        ].map((item, index) => (
          <Box key={index}>
            <item.icon />
            <span> {item.label}</span>
          </Box>
        ))}

        <p>{business_description}</p>
      </Stack>
      <div style={{
        maxHeight: '1200px',
        height: '100%',
        backgroundColor: "rgba(141, 108, 159, 1)",
        color: "white",
      }}>
        <Stack
          spacing={5}
          sx={{
            padding: "20px",
          }}
        >
          {["SERVICES", "PHOTOS", "REVIEW"].map((item, index) => (
            <Button key={index} variant="text" color="inherit">
              {item.toUpperCase()}
            </Button>
          ))}
        </Stack>
      </div>
    </Box>
  );
}

export default SideDrawer;
