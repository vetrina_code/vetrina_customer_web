import React from "react";
import { Box, Paper, Grid, Rating, Stack } from "@mui/material";

function ReviewCard() {
  return (
    <Box
      sx={{
        width: "311px",
        fontSize: "11px",
      }}
    >
      <Paper
        elevation={5}
        sx={{
          padding: 2,
        }}
      >
        <Stack spacing={1}>
          <Grid
            container
            direction="row"
            justifyContent="space-between"
            alignItems="baseline"
          >
            <Rating precision={0.5} defaultValue={2} readOnly/>
            <span>2 days ago</span>
          </Grid>
          <h4>Business Name</h4>
          <Box width={'85%'} sx={{
            height: "70px",
            overflow: 'hidden',
          }}>
            <span>Lorem ipsum dolor sit amet consectetur adipisicing elit. Laboriosam sint, maiores distinctio, eum culpa quos hic dicta odit reprehenderit magnam repudiandae. Dolores veniam dicta ab sed cum distinctio. Modi, delectus!</span>
          </Box>
        </Stack>
      </Paper>
    </Box>
  );
}

export default ReviewCard;
