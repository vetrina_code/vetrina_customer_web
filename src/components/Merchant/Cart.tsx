import React from "react";
import { Box, Paper, Grid, Stack } from "@mui/material";
import { FillButton } from "../../ui/Buttons";

const options = (service: string, amount: number) => {
  return (
    <Box
      sx={{
        padding: 1,
        fontSize: "11px",
        fontWeight: "600",
      }}
    >
      <Grid container justifyContent="space-between">
        <span>{service}</span>
        <span>${amount}</span>
      </Grid>
      <Stack direction="row" spacing={2}>
        <span>Edit</span>
        <span>Remove</span>
      </Stack>
    </Box>
  );
};

function Cart() {
  const cartItems = [
    {
      service: "Service 1",
      price: "100",
    },
    {
      service: "Service 2",
      price: "100",
    },
    {
      service: "Service 3",
      price: "100",
    },
  ];
  return (
    <Stack spacing={3}>
      <Box
        sx={{
          width: "311px",
        }}
      >
        <Paper
          elevation={5}
          sx={{
            padding: 2,
            textAlign: "right",
            fontSize: "15px",
            fontWeight: "bold",
            fontFamily: "Montserrat",
          }}
        >
          <Stack spacing={3}>
            <span>"Cart"</span>
            {cartItems.map((item, index) => {
              return (
                <div key={index}>
                  {options(item.service, Number(item.price))}
                </div>
              );
            })}
          </Stack>
        </Paper>
      </Box>
      <Box
        sx={{
          width: "311px",
        }}
      >
        <Paper
          elevation={5}
          sx={{
            padding: 2,
            fontFamily: "Open Sans",
            fontSize: "21px",
            fontWeight: "600",
            lineHeight: "22px",
            letterSpacing: "0em",
            textAlign: "left",
          }}
        >
          <Stack spacing={3} alignItems="center">
            <Grid container justifyContent={"space-between"}>
              <span>Total</span>
              <span>
                ${cartItems.reduce((sum, item) => sum + Number(item.price), 0)}
              </span>
            </Grid>
            <FillButton
              sx={{
                fontSize: "14px",
                // padding: "0px 2px !important",
                color: "white",
                textTransform: "uppercase !important",
              }}
            >
              <span>Checkout</span>
            </FillButton>
          </Stack>
        </Paper>
      </Box>
    </Stack>
  );
}

export default Cart;
