import React from "react";
import { Breadcrumbs, Typography, Link } from "@mui/material";
import { MarketPlaceBusinessI } from "../../interfaces/slices";

function MerchantLocation({ business }: { business: MarketPlaceBusinessI }) {
  const {
    business_name,
    category
  } = business;
  return (
    <Breadcrumbs>
      <Link underline="hover" color="inherit" href="/">
        Location
      </Link>
      <Link
        underline="hover"
        color="inherit"
        href={'/marketplace?category=' + category.name}
      >
        {category.name}
      </Link>
      <Typography color="text.primary">{business_name}</Typography>
    </Breadcrumbs>
  );
}

export default MerchantLocation;
