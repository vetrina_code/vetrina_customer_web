import React from "react";
import { Grid, Box, Paper } from "@mui/material";
import { VerifiedIcon } from "../../assets/svg";

function BusinessHeader(business: any) {
  return (
    <div
      style={{
        background:
          "url(https://images.unsplash.com/photo-1518791841217-8f162f1e1131?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80) no-repeat center center",
        backgroundSize: "cover",
        backgroundPosition: "center",
        width: "100%",
        height: "33vh",
      }}
    >
      <Grid
        container
        justifyContent={"space-between"}
        direction="row"
        padding={"40px"}
      >
        <Box>
          <VerifiedIcon />
        </Box>
        <Box>
          <Paper
            elevation={3}
            sx={{
              background: "rgba(141, 108, 159, 1)",
              padding: "20px",
              color: "white",
            }}
          >
            <span>Open Monday through Friday</span>
            <br />
            <span>10am to 9pm</span>
          </Paper>
        </Box>
      </Grid>
    </div>
  );
}

export default BusinessHeader;
