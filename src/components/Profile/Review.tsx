import React from "react";
import { ReviewModal } from "../Modals";
import {
  Grid,
  Typography,
  Stack,
  FormControl,
  InputAdornment,
} from "@mui/material";
import { SearchInput } from "./Input";
import { SearchOff } from "@mui/icons-material";
import ReviewCard from "./ReviewCard";
import { SearchIconOutlined } from "../../assets/svg";

const review = [
  {
    name: "Merchant 1",
    rating: 4.5,
    services: ["Service 1", "Service 2", "Service 3"],
  },
  {
    name: "Merchant 2",
    rating: 4.5,
    services: ["Service 1", "Service 2", "Service 3"],
  },
  {
    name: "Merchant 3",
    rating: 4.5,
    services: ["Service 1", "Service 2", "Service 3"],
  },
  {
    name: "Merchant 4",
    rating: 4.5,
    services: ["Service 1", "Service 2", "Service 3"],
  },

]

function Review() {
  const [reviewModal, setReviewModal] = React.useState(false);
  const [search, setSearch] = React.useState("");
  const handleSetSearch = (event: React.ChangeEvent<HTMLInputElement>) =>
    setSearch(event.currentTarget.value);
  
  const handleReview = () => setReviewModal(true);
  return (
    <div>
      <ReviewModal
        open={reviewModal}
        handleClose={() => setReviewModal(false)}
      />
      <Stack spacing={3}>
        <Grid
          container
          justifyContent={"space-between"}
          sx={{
            maxWidth: "700px",
          }}
        >
          <Typography variant={"h5"} fontFamily="monospace">
            Merchants Patronized
          </Typography>
          <FormControl variant="standard" sx={{
            maxWidth:'300px',
            width:'100%',
          }}>
            <SearchInput
              name="search"
              value={search}
              type="text"
              id="new-password-input"
              onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
                handleSetSearch(event)
              }
              startAdornment={
                <InputAdornment position="end">
                <SearchIconOutlined />
              </InputAdornment>
              }
              placeholder="Search"
            />
          </FormControl>
        </Grid>

        <Grid container spacing={5}>
          {
            review.map((item, index) => (
              <Grid item xs={12} sm={6} md={4} lg={3} key={index}>
                <ReviewCard key={index} item={item} handleReview={handleReview}/>
              </Grid>
            ))
          }
        </Grid>
      </Stack>
    </div>
  );
}

export default Review;
