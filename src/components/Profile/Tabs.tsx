import {styled} from "@mui/material";
import {
  Tabs,
  Tab,
  Box,
  Typography
} from "@mui/material";

export const CustomTabs = styled(Tabs)({
  fontFamily: 'Montserrat',
  '& .MuiTabs-indicator': {
    backgroundColor: 'black',
  },
});

export const CustomTab = styled(Tab)({
  textTransform: 'none',
  '& ,Mui-selected': {
    color: 'black !important',
    fontWeight: 700,
  },
});
interface TabPanelProps {
  children?: React.ReactNode;
  index: number;
  value: number;
}

export function TabPanel(props: TabPanelProps) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          {children}
        </Box>
      )}
    </div>
  );
}