import React from "react";
import {
  GetUserResponse,
  useGetProfileQuery,
  useUpdateProfileMutation,
} from "../../service/varroe/user/api";
import { selectAppState, setAppState } from "../../features/App";
import {
  Container,
  Stack,
  FormControl,
  Button,
  InputLabel,
} from "@mui/material";
import { FillButton } from "../../ui/Buttons";
import { ProfileInput } from "./Input";
import { UserT } from "../../interfaces/slices";
import { useAppDispatch, useAppSelector } from "../../app/store";

function Info() {
  const dispatch = useAppDispatch();
  const { data, isLoading, error } = useGetProfileQuery();
  const [
    updateProfile,
    { isLoading: isUpdating, error: updateError, data: updateData },
  ] = useUpdateProfileMutation();
  const [response, setResponse] = React.useState<GetUserResponse>();
  const [values, setValue] = React.useState<UserT>({
    first_name: "",
    last_name: "",
    email: "",
  });

  React.useEffect(() => {
    if (data || updateData) {
      setResponse(data || updateData);
    }
  }, [data, updateData]);

  React.useEffect(() => {
      dispatch(
        setAppState({
          success: undefined,
          error: error ? (error as any).data.message : undefined,
          isLoading,
          isError: error ? true : false,
          isSuccess: false,
        })
      );
  }, [dispatch, isLoading, error]);

  React.useEffect(() => {
    if (response) {
      const { email } = response.data;
      setValue((prev) => ({
        ...prev,
        email,
      }));
    }
  }, [response]);

  const handleSetValue = (event: React.ChangeEvent<HTMLInputElement>) => {
    setValue({
      ...values,
      [event.currentTarget.name]: event.currentTarget.value,
    });
  };

  const handleSubmit = async () => await updateProfile(values);

  return (
    <Container
      sx={{
        fontFamily: "Montserrat !important",
      }}
    >
      <Stack spacing={5}>
        <Stack direction="row" spacing={4}>
          <img
            src={
              "https://images.unsplash.com/photo-1518791841217-8f162f1e1131?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80"
            }
            alt="avatar"
            width={80}
            height={80}
            style={{
              borderRadius: "50%",
            }}
          />
          <FillButton
            sx={{
              marginTop: "4rem",
            }}
            textcolor="white"
          >
            UPLOAD
          </FillButton>
          <FillButton
            mycolor="red"
            textcolor="white"
            sx={{
              marginTop: "3rem",
            }}
          >
            DELETE
          </FillButton>
        </Stack>
        <Stack direction="row" spacing={4}>
          <FormControl
            variant="standard"
            size="small"
            sx={{
              maxWidth: "300px",
              display: "flex",
            }}
          >
            <InputLabel shrink htmlFor="first-name-input">
              First Name
            </InputLabel>
            <ProfileInput
              name="first_name"
              type="text"
              value={values.first_name}
              id="first-name-input"
              onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
                handleSetValue(event)
              }
            />
          </FormControl>
          <FormControl
            variant="standard"
            size="small"
            sx={{
              maxWidth: "300px",
              display: "flex",
            }}
          >
            <InputLabel shrink htmlFor="last-name-input">
              Last Name
            </InputLabel>
            <ProfileInput
              name="last_name"
              type="text"
              value={values.last_name}
              id="last-name-input"
              onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
                handleSetValue(event)
              }
            />
          </FormControl>
        </Stack>
        <Stack direction="row" spacing={4}>
          <FormControl
            variant="standard"
            size="small"
            sx={{
              maxWidth: "300px",
              display: "flex",
            }}
          >
            <InputLabel shrink htmlFor="first-name-input">
              Email
            </InputLabel>
            <ProfileInput
              name="email"
              type="email"
              value={values.email}
              id="email-input"
              onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
                handleSetValue(event)
              }
            />
          </FormControl>
          <FormControl
            variant="standard"
            size="small"
            sx={{
              maxWidth: "300px",
              display: "flex",
            }}
          >
            <InputLabel shrink htmlFor="last-name-input">
              Phone Number
            </InputLabel>
            <ProfileInput
              name="phone"
              type="text"
              value={values.email}
              id="phone-input"
              onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
                handleSetValue(event)
              }
            />
            <Button
              variant="contained"
              sx={{
                textTransform: "uppercase !important",
                color: "white",
                background: "rgba(133, 133, 133, 1);",

                // width: '70%',
                float: "right",
                fontSize: "12px",
                fontWeight: 700,
                fontFamily: "Montserrat",
                marginTop: "1rem",
              }}
              onClick={handleSubmit}
            >
              update password
            </Button>
          </FormControl>
        </Stack>
      </Stack>
    </Container>
  );
}

export default Info;
