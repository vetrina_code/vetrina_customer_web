import {
  Input,
  InputBase,
  styled,
} from '@mui/material';

export const ProfileInput = styled(InputBase)(({ theme }) => ({
  "label + &": {
    marginTop: theme.spacing(3),
  },
  "& .MuiInputBase-input": {
    borderRadius: 4,
    position: "relative",
    backgroundColor: theme.palette.mode === "light" ? "#fcfcfb" : "#2b2b2b",
    border: "1px solid #ced4da",
    fontSize: 16,
    width: "auto",
    padding: "10px 12px",
    transition: theme.transitions.create([
      "border-color",
      "background-color",
      "box-shadow",
    ]),
    "&:focus": {
      // boxShadow: `${alpha(theme.palette.primary.main, 0.25)} 0 0 0 0.2rem`,
      borderColor: theme.palette.primary.main,
    },
  },
}));

export const SearchInput = styled(Input)(({ theme }) => ({
  borderRadius: 4,
  border: '0.5px solid rgb(204, 204, 204)',
  fontSize: 16,
  fontFamily: 'Montserrat',
  background: "rgba(240, 240, 240, 1)",
  padding: '3px',
  '&:before': {
    border: 'none',
  },
  '&:after': {
    border: 'none',
  },
}));