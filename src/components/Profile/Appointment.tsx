import { SearchOff } from "@mui/icons-material";
import {
  Stack,
  FormControl,
  Grid,
  InputAdornment,
  InputLabel,
  MenuItem,
  Select,
  SelectChangeEvent,
  IconButton,
} from "@mui/material";
import React from "react";
import { CalenderIcon, SearchIcon, SearchIconOutlined } from "../../assets/svg";
import { SearchInput } from "./Input";
type View = "table" | "calender";
function Appointment() {
  const [search, setSearch] = React.useState("");
  const [duration, setDuration] = React.useState("6");
  const [view, setView] = React.useState<View>("calender");
  const handleSetSearch = (event: React.ChangeEvent<HTMLInputElement>) =>
    setSearch(event.currentTarget.value);

  const handleSetDuration = (event: SelectChangeEvent<string>) =>
    setDuration(event.target.value);

  const handleSetView = (view: View) => setView(view);
  return (
    <Grid container>
      <Grid container justifyContent={"space-between"}>
        <FormControl
          variant="standard"
          sx={{
            maxWidth: "300px",
            width: "100%",
          }}
        >
          <SearchInput
            name="search"
            value={search}
            type="text"
            id="new-password-input"
            onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
              handleSetSearch(event)
            }
            startAdornment={
              <InputAdornment position="end">
                <SearchIconOutlined />
              </InputAdornment>
            }
            placeholder=" Search"
          />
        </FormControl>
        <Stack direction="row" spacing={3}>
          {view === "calender" ? (
            <IconButton onClick={() => handleSetView('table')}>
              <CalenderIcon />
            </IconButton>
          ) :
            <IconButton onClick={() => handleSetView('calender')}>
              <CalenderIcon />
            </IconButton>
          }
          <FormControl
            sx={{ m: 1, minWidth: 120, padding: 0, margin: 0 }}
            size="small"
          >
            <Select
              labelId="duration-select-small"
              id="duration-select-small"
              value={duration}
              label=""
              onChange={(event: SelectChangeEvent<string>) =>
                handleSetDuration(event)
              }
            >
              <MenuItem value={6}>Past 6 Months</MenuItem>
              <MenuItem value={12}>Past 12 Months</MenuItem>
              <MenuItem value={-1}>Upcoming</MenuItem>
            </Select>
          </FormControl>
        </Stack>
      </Grid>
    </Grid>
  );
}

export default Appointment;
