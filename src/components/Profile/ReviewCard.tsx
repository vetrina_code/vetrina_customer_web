import React from "react";
import {
  Box,
  Paper,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  styled,
  Divider,
  ListItemButton,
} from "@mui/material";

export const CustomListItemText = styled(ListItemText)(() => ({
  "& .MuiListItemText-primary": {
    fontSize: "12px",
    fontFamily: "Montserrat",
  },
}));
export const CustomListItem = styled(ListItem)(() => ({
  padding: "0px 4px",
}));
function ReviewCard({ item, handleReview }: any) {
  return (
    <Box
      sx={{
        maxWidth: "300px",
        width: "100%",
        fontSize: "8px",
      }}
    >
      <Paper
        elevation={3}
        sx={{
          border: "rgba(240, 240, 240, 1) 1px solid",
        }}
      >
        <List
          sx={{
            padding: "0px",
          }}
        >
          <CustomListItem>
            <ListItemIcon>
              <img src={""} alt="" width="50px" height={"50px"} />
            </ListItemIcon>
            <CustomListItemText
              primary={item.name}
              sx={{
                "& .MuiListItemText-primary": {
                  fontWeight: "bold",
                  fontSize: "15px",
                },
              }}
            />
          </CustomListItem>
          <CustomListItem
            sx={{
              background: "#f5f5f5",
            }}
          >
            <CustomListItemText
              primary={"Services Patronized"}
              sx={{
                "& .MuiListItemText-primary": {
                  fontWeight: "bold",
                },
              }}
            />
          </CustomListItem>
          {item.services.map((service: string, index: number) => (
            <>
              <CustomListItem
                key={index}
                secondaryAction={
                  <ListItemButton onClick={() => handleReview()}>
                    Submit Review
                  </ListItemButton>
                }
              >
                <CustomListItemText primary={service} />
              </CustomListItem>
              <Divider />
            </>
          ))}
        </List>
      </Paper>
    </Box>
  );
}

export default ReviewCard;
