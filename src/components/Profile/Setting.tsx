import React from "react";
import {
  Typography,
  Stack,
  Container,
  FormControl,
  InputLabel,
  FormHelperText,
  Button
} from "@mui/material";
import { ProfileInput } from "./Input";

function Setting() {
  const [values, setValue] = React.useState({
    currentPassword: "",
    newPassword: "",
  });
  const handleSetValue =  (event: React.ChangeEvent<HTMLInputElement>) => {
    setValue({ ...values, [event.currentTarget.name]: event.currentTarget.value });
  }
  return (
    <>
      <Stack spacing={3}>
        <Typography variant="h5">Manage your Password</Typography>
        <Stack direction={"row"} spacing={5}>
          <FormControl variant="standard">
            <InputLabel shrink htmlFor="current-password-input">
              Current Password
            </InputLabel>
            <ProfileInput
             name="currentPassword"
             value={values.currentPassword}
             type="password"
             id="new-password-input"
             onChange={(event: React.ChangeEvent<HTMLInputElement>) => handleSetValue(event)}
            />
          </FormControl>
          <FormControl variant="standard" size='small' sx={{
            maxWidth:'300px',
            display: 'flex',
          }}>
            <InputLabel shrink htmlFor="new-password-input">
              New Password
            </InputLabel>
            <ProfileInput
              name="newPassword"
              type="password"
              value={values.newPassword}
              id="new-password-input"
              onChange={(event: React.ChangeEvent<HTMLInputElement>) => handleSetValue(event)}
            />
            <FormHelperText>
            For stronger security, your new password must be at least 8 characters long and include at least 1 special character and 1 number
            </FormHelperText>
            <Button
            variant="contained"
            sx={{
              textTransform: 'uppercase !important',
              color: 'white',
              background: "rgba(133, 133, 133, 1);",

              // width: '70%',
              float: 'right',
              fontSize: '12px',
              fontWeight: 700,
              fontFamily: 'Montserrat',
              marginTop: '1rem',
            }}>update password</Button>
          </FormControl>
        </Stack>
      </Stack>
    </>
  );
}

export default Setting;
