import React from "react";
import {
  Box,
  Select,
  SelectChangeEvent,
  InputLabel,
  MenuItem,
  FormControl,
  TextField,
  Typography,
} from "@mui/material";
import LocationIcon from "../../assets/svg/LocationIcon";
import SearchIcon from "../../assets/svg/SearchIcon";
import { Input } from "../../ui/Form/Input";

function Search() {
  const [location, setLocation] = React.useState("ON");
  const [searchValue, setSearchValue] = React.useState("");
  const conutryMap: Record<string, string> = {
    CA: "Canada",
    ON: "Ontario",
    QC: "Quebec",
    AB: "Alberta",
    BC: "British Columbia",
    MB: "Manitoba",
    NB: "New Brunswick",
    NL: "Newfoundland and Labrador",
    NS: "Nova Scotia",
    PE: "Prince Edward Island",
  };

  const handleLocationChange = (event: SelectChangeEvent) => {
    setLocation(event.target.value);
  };
  const handleSearchValueChange = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setSearchValue(event.target.value);
  };
  return (
    <Box
      sx={{
        overflow: "hidden",
        display: "flex",
        height: "54px",
        width: "100%",
        maxWidth: "726px",
        borderRadius: "10px",

        boxShadow: "0px 3px 23px 0px rgba(0, 0, 0, 0.25)",
        border: "1px solid purple",
        background: "white",
        fontFamily: "Montserrat",
        fontSize: "11px",
        fontWeight: 700,
        LineHeight: "13.4px",
      }}
    >
      <Box
        sx={{
          padding: "10px",
          display: "flex",
          float: "left",
          width: "95%",
        }}
      >
        <Box
          sx={{
            position: "relative",
          }}
        >
          <LocationIcon height={"100%"} width={"20px"} />
        </Box>
        <Box
          sx={{
            width: "100%",
            maxWidth: "130px",
            borderRight: "1px solid black;",
            color: "#9fa3b1;",
            position: "relative;",
            cursor: "pointer;",
            overflow: "hidden",
          }}
        >
          <FormControl variant="standard" sx={{ m: 1, minWidth: 100 }}>
            <Select
              labelId="demo-simple-select-standard-label"
              id="demo-simple-select-standard"
              value={location}
              onChange={handleLocationChange}
              autoWidth
              renderValue={(value: string) => `${conutryMap[value]}, ${value}`}
              label="Location"
              variant="standard"
              sx={{
                fontFamily: "Open Sans",
                fontSize: "12px",
                border: "none",
                "&:before": {
                  border: "none",
                },
                "&:after": {
                  border: "none",
                },
              }}
            >
              <MenuItem value={"ON"}>TORONTO</MenuItem>
              <MenuItem value={"QC"}>MONTREAL</MenuItem>
              <MenuItem value={"AB"}>ALBERTA</MenuItem>
              <MenuItem value={"BC"}>BRITISH COLUMBIA</MenuItem>
              <MenuItem value={"MB"}>MANITOBA</MenuItem>
              <MenuItem value={"NB"}>NEW BRUNSWICK</MenuItem>
              <MenuItem value={"NL"}>NEWFOUNDLAND AND LABRADOR</MenuItem>
              <MenuItem value={"NS"}>NOVA SCOTIA</MenuItem>
              <MenuItem value={"PE"}>PRINCE EDWARD ISLAND</MenuItem>
            </Select>
          </FormControl>
        </Box>
        <Box
          sx={{
            width: "100%",
            display: "flex",
            padding: "8px",
          }}
        >
          <Typography
            component="span"
            sx={{
              marginRight: '3px',
              color: 'black',
              fontFamily: "Montserrat !important",
              fontWeight: 700,
              fontSize: "0.75rem",
              // display: "block",
            }}
          >
            DISCOVER
          </Typography>

          <Box
            sx={{
              width: "100%",
              display: "flex",
            }}
          >
            <Input
              placeholder="Photographers, plumbers, salons"
              value={searchValue}
              onChange={(e) =>
                handleSearchValueChange(
                  e as React.ChangeEvent<HTMLInputElement>
                )
              }
            />
          </Box>
        </Box>
      </Box>
      <Box
        sx={{
          display: "flex",
          justifyContent: "flex-end",
          float: "right",
          // height: "100%",
        }}
      >
        <SearchIcon height={"54px"} width={"54px"} />
      </Box>
    </Box>
  );
}

export default Search;
