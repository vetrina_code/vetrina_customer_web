import { Card } from "@mui/material";
import styled from "styled-components";

export const ExpandableCard = styled(Card)<{ expand: Boolean, shadow?: boolean }>(
  ({ expand, shadow }) => `
    width: ${expand ? "545px" : "445px"};
    background-color: black;
    box-shadow: ${ shadow && "0px 0px 10px rgba(0, 0, 0, 0.50)" };
  `
);
