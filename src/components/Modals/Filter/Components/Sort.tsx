import React from "react";
import { FormControlLabel, FormGroup, Checkbox, styled } from "@mui/material";
import { CheckCircleIcon, CheckCircleIconFilled } from "../../../../assets/svg";


const BootStrapFormControlLabel = styled(FormControlLabel)(() => ({
  "& .MuiFormControlLabel-label": {
    fontFamily: "Montserrat",
    fontSize: "10px",
    fontWeight: 400,
    LineHeight: "12px",
  },
}));

export default function SortbyComponent(): JSX.Element {
  interface SortSelection {
    relevance?: boolean;
    freeLancers?: boolean;
    VarroeVerified?: boolean;
    costLowToHigh?: boolean;
    costHighToLow?: boolean;
  }

  const [sortSelection, setSortSelection] = React.useState<SortSelection>({});
  const handleSortSelection = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSortSelection({
      // ...sortSelection,
      [event.target.name]: event.target.checked,
    });
  };

  return (
    <>
      <span>Sort By</span>
      <FormGroup>
        <BootStrapFormControlLabel
          label="Relevance"
          control={
            <Checkbox
              name="relevance"
              checked={sortSelection.relevance}
              onChange={(e) => handleSortSelection(e)}
              icon={<CheckCircleIcon />}
              checkedIcon={<CheckCircleIconFilled />}
            />
          }
        />
        <BootStrapFormControlLabel
          label="Freelancers"
          control={
            <Checkbox
              name="freeLancers"
              checked={sortSelection.freeLancers}
              onChange={(e) => handleSortSelection(e)}
              icon={<CheckCircleIcon />}
              checkedIcon={<CheckCircleIconFilled />}
            />
          }
        />
        <BootStrapFormControlLabel
          label="Varroe Verified"
          control={
            <Checkbox
              name="VarroeVerified"
              checked={sortSelection.VarroeVerified}
              onChange={(e) => handleSortSelection(e)}
              icon={<CheckCircleIcon />}
              checkedIcon={<CheckCircleIconFilled />}
            />
          }
        />
        <BootStrapFormControlLabel
          label="Cost: Low to High"
          control={
            <Checkbox
              name="costLowToHigh"
              checked={sortSelection.costLowToHigh}
              onChange={(e) => handleSortSelection(e)}
              icon={<CheckCircleIcon />}
              checkedIcon={<CheckCircleIconFilled />}
            />
          }
        />
        <BootStrapFormControlLabel
          label="Cost: High to Low"
          control={
            <Checkbox
              name="costHighToLow"
              checked={sortSelection.costHighToLow}
              onChange={(e) => handleSortSelection(e)}
              icon={<CheckCircleIcon />}
              checkedIcon={<CheckCircleIconFilled />}
            />
          }
        />
      </FormGroup>
    </>
  );
}