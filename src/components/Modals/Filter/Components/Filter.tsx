import { Box, Slider, styled } from "@mui/material";

export default function FilterByComponent(): JSX.Element {
  const iOSBoxShadow =
    "0 3px 1px rgba(0,0,0,0.1),0 4px 8px rgba(0,0,0,0.13),0 0 0 1px rgba(0,0,0,0.02)";
  const FilterSlider = styled(Slider)(({ theme }) => ({
    color: theme.palette.mode === "dark" ? "#5C0E90" : "#5C0E90",
    height: 2,
    padding: "15px 0",
    "& .MuiSlider-thumb": {
      height: 17,
      width: 17,
      marginRight: 8,
      backgroundColor: "#5C0E90",
      boxShadow: iOSBoxShadow,
      "&:focus, &:hover, &.Mui-active": {
        boxShadow:
          "0 3px 1px rgba(0,0,0,0.1),0 4px 8px rgba(0,0,0,0.3),0 0 0 1px rgba(0,0,0,0.02)",
        // Reset on touch devices, it doesn't add specificity
        "@media (hover: none)": {
          boxShadow: iOSBoxShadow,
        },
      },
    },
    "& .MuiSlider-valueLabel": {
      fontFamily: "Montserrat",
      lineHeight: "19px",
      letterSpacing: "0em",
      textAlign: "left",
      fontSize: 14,
      fontWeight: 600,
      top: -6,
      backgroundColor: "unset",
      color: theme.palette.text.primary,
      "&:before": {
        display: "none",
      },
      "& *": {
        background: "transparent",
        color: theme.palette.mode === "dark" ? "#fff" : "#000",
      },
    },
    "& .MuiSlider-track": {
      border: "none",
    },
    "& .MuiSlider-rail": {
      opacity: 0.5,
      backgroundColor: "#bfbfbf",
      height: "3px",
    },
    "& .MuiSlider-mark": {
      backgroundColor: "#bfbfbf",
      height: 17,
      width: 17,
      borderRadius: 50,
      "&.MuiSlider-markActive": {
        opacity: 1,
        backgroundColor: "currentColor",
      },
    },
    "& .MuiSlider-markLabel": {
      paddingLeft: 15,
      fontFamily: "Montserrat",
      lineHeight: "19px",
      letterSpacing: "0em",
      textAlign: "left",
      fontSize: 14,
      fontWeight: 600,
    },
  }));
  const priceMarks = [
    {
      value: 0,
      label: "$",
    },
    {
      value: 25,
      label: "$$",
    },
    {
      value: 50,
      label: "$$$",
    },
    {
      value: 75,
      label: "$$$$",
    },
    {
      value: 100,
      label: "$$$$$",
    },
  ];
  const ratingMarks = [
    {
      value: 0,
      label: "1 \nRating",
    },
    {
      value: 25,
      label: "2 \nRating",
    },
    {
      value: 50,
      label: "3 \nRating",
    },
    {
      value: 75,
      label: "4 \nRating",
    },
    {
      value: 100,
      label: "5 \nRating",
    },
  ];
  return (
    <>
      <span>Average Price</span>
      <Box
        sx={{
          marginTop: "10px",
        }}
      >
        <FilterSlider
          size="small"
          defaultValue={0}
          marks={priceMarks}
          // step={marks.length * 2}
        />
      </Box>
      <span>Ratings</span>
      <Box
        sx={{
          marginTop: "10px",
        }}
      >
        <FilterSlider
          size="small"
          defaultValue={0}
          marks={ratingMarks}
          // step={marks.length * 2}
        />
      </Box>
    </>
  );
}