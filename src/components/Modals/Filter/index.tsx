import * as React from "react";
import {
  Dialog,
  DialogContent,
  DialogTitle,
  Grid,
  IconButton,
  styled,
} from "@mui/material";

import { SortBy, FilterBy } from "./Components";
import {
  BlackCloseIcon,
} from "../../../assets/svg";
import { ModalTitleText } from "../components";
import { ModalProps } from "../../../interfaces/designs";

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  "& .MuiDialogContent-root": {
    padding: theme.spacing(2),
  },
  "& .MuiDialogActions-root": {
    padding: theme.spacing(1),
  },
  fontFamily: "Montserrat",
  // maxWidth: "738px",
  // width: "calc(100% - 25%)",
}));
const BootstrapDialogContent = styled(DialogContent)(() => ({
  "& .MuiDialogContent-root": {},
  padding: "24px !important",
  fontFamily: "Montserrat",
  fontSize: "18px",
  fontWeight: 600,
  lineHeight: "24px",
}));

export default function FilterModal(props: ModalProps) {

  return (
    <BootstrapDialog onClose={() => props.onClose()} open={props.open} fullWidth>
      <DialogTitle>
        <ModalTitleText>
          Filters
        </ModalTitleText>
        <IconButton
          onClick={() => props.onClose()}
          sx={{
            position: "absolute",
            top: "20px",
            right: "10px",
          }}
        >
          <BlackCloseIcon />
        </IconButton>
      </DialogTitle>
      <BootstrapDialogContent>
        <Grid container>
          <Grid item md={3}>
            <SortBy />
          </Grid>
          <Grid item md={8}>
            <FilterBy />
          </Grid>
        </Grid>
      </BootstrapDialogContent>
    </BootstrapDialog>
  );
}
