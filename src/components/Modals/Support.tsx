import React from "react";
import {
  Card,
  CardActionArea,
  CardActions,
  CardContent,
  CardHeader,
  IconButton,
  Typography,
} from "@mui/material";
import {
  SupportLogo,
  MessageIcon,
  MailIcon,
  PhoneIcon,
} from "../../assets/svg";
import styled from "styled-components";
import { ExpandableCard } from "../Layout/Card";

function SupportModal() {
  return (
    <ExpandableCard
      expand={true}
      variant="outlined"
      sx={{
        background: "rgba(37, 29, 42, 1);",
        color: "white",
        textAlign: "center",
        width: "284px",
      }}
    >
      <CardHeader
        avatar={<SupportLogo />}
        title={
          <Typography
            variant="h6"
            sx={{ fontSize: "16px !important", fontFamily: "Montserrat" }}
          >
            Got questions? We have answers.
          </Typography>
        }
      />
      <CardContent>
        <Typography variant="caption" sx={{ fontFamily: "Montserrat" }}>
        Reach out to us with any of the methods below
        </Typography>
      </CardContent>
      <CardActionArea sx={{background: "white"}}>
        <CardActions sx={{justifyContent: "center"}}>
          <IconButton>
            <MessageIcon />
          </IconButton>
          <IconButton>
            <MailIcon />
          </IconButton>
          <IconButton>
            <PhoneIcon />
          </IconButton>
        </CardActions>
      </CardActionArea>
    </ExpandableCard>
  );
}

export default SupportModal;
