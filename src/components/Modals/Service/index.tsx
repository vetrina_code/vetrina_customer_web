import React from "react";
// Core Compoments
import {
  CustomizeOption,
  DateOption,
  TimeOption,
  ConfirmedOption,
  OptionDetails,
  ExtrasOption,
} from "./Options";
// Design & Icon Components
import {
  Dialog,
  DialogActions,
  DialogContent,
  Grid,
  MobileStepper,
  Stack,
  styled,
} from "@mui/material";
// Design Interfaces
import {
  SelectedServiceOptions,
  ServiceSteps,
  BusinessDetails,
} from "../../../interfaces/designs";
import { useAppSelector } from "../../../app/store";
import { selectService } from "../../../features/Merchant";

const CustomStepper = styled(MobileStepper)(() => ({
  "& .MuiMobileStepper-dot": {
    backgroundColor: "black",
    width: "3px",
    height: "3px",
  },
  "& .MuiMobileStepper-dotActive": {
    width: "20px",
    borderRadius: "50px",
    border: "1px solid black",
  },
}));

const ContentBlock = styled(DialogContent)(() => ({
  padding: "0px",
  justifyContent: "space-evenly",
}));

function Servicemodal({
  open,
  handleClose,
  businessKey,
}: {
  open: boolean;
  handleClose: () => void;
  businessKey: boolean;
}) {
  const [step, setActiveStep] = React.useState<ServiceSteps>("customize");
  const business1: BusinessDetails = {
    name: "Business 1 Name",
    address: "Business 1 Location",
    phone: "416-123-4567",
    website: "www.business1.com",
    service: {
      name: "Service 1",
      description: "Service 1 description",
      price: 100,
      duration: 60,
      currency: "$",
      options: [
        {
          label: "Option 1",
          values: [
            { label: "Value 1", price: 10 },
            { label: "Value 2", price: 20 },
            { label: "Value 3", price: 30 },
          ],
        },
        {
          label: "Option 2",
          values: [
            { label: "Value 1", price: 10 },
            { label: "Value 2", price: 20 },
            { label: "Value 3", price: 30 },
          ],
        },
        {
          label: "Option 3",
          values: [
            { label: "Value 1", price: 10 },
            { label: "Value 2", price: 20 },
            { label: "Value 3", price: 30 },
          ],
        },
      ],
    },
    availableDates: [
      {
        date: "2022-06-06T11:27:12.141Z",
        availablePeople: [
          {
            name: "Person 1",
            price: 10,
            isAvailable: true,
            image: "",
            availableTime: [
              {
                start: "9:00",
                end: "10:30",
              },
              {
                start: "10:45",
                end: "12:15",
              },
              {
                start: "12:30",
                end: "02:00",
              },
            ],
            availableDate: [
              "2022-06-05T11:27:12.141Z",
              "2022-06-06T11:27:12.141Z",
              "2022-06-07T11:27:12.141Z",
            ],
          },
          {
            name: "Person 2",
            price: 20,
            isAvailable: true,
            image: "",
            availableTime: [
              {
                start: "9:00",
                end: "10:30",
              },
              {
                start: "10:45",
                end: "12:15",
              },
              {
                start: "12:30",
                end: "02:00",
              },
            ],
            availableDate: [
              "2022-06-05T11:27:12.141Z",
              "2022-06-06T11:27:12.141Z",
              "2022-06-07T11:27:12.141Z",
            ],
          },
          {
            name: "Person 3",
            price: 30,
            isAvailable: true,
            image: "",
            availableTime: [
              {
                start: "9:00",
                end: "10:30",
              },
              {
                start: "10:45",
                end: "12:15",
              },
              {
                start: "12:30",
                end: "02:00",
              },
            ],
            availableDate: [
              "2022-06-05T11:27:12.141Z",
              "2022-06-06T11:27:12.141Z",
              "2022-06-07T11:27:12.141Z",
            ],
          },
          {
            name: "Person 4",
            price: 40,
            isAvailable: true,
            image: "",
            availableTime: [
              {
                start: "9:00",
                end: "10:30",
              },
              {
                start: "10:45",
                end: "12:15",
              },
              {
                start: "12:30",
                end: "02:00",
              },
            ],
            availableDate: [
              "2022-06-05T11:27:12.141Z",
              "2022-06-06T11:27:12.141Z",
              "2022-06-07T11:27:12.141Z",
            ],
          },
          {
            name: "Person 5",
            price: 50,
            isAvailable: true,
            image: "",
            availableTime: [
              {
                start: "9:00",
                end: "10:30",
              },
              {
                start: "10:45",
                end: "12:15",
              },
              {
                start: "12:30",
                end: "02:00",
              },
            ],
            availableDate: [
              "2022-06-05T11:27:12.141Z",
              "2022-06-06T11:27:12.141Z",
              "2022-06-07T11:27:12.141Z",
            ],
          },
        ],
      },
      {
        date: "2022-06-07T11:27:12.141Z",
        availablePeople: [
          {
            name: "Person 1",
            price: 10,
            isAvailable: true,
            image: "",
            availableTime: [
              {
                start: "9:00",
                end: "10:30",
              },
              {
                start: "10:45",
                end: "12:15",
              },
              {
                start: "12:30",
                end: "02:00",
              },
            ],
            availableDate: [
              "2022-06-05T11:27:12.141Z",
              "2022-06-06T11:27:12.141Z",
              "2022-06-07T11:27:12.141Z",
            ],
          },
          {
            name: "Person 2",
            price: 20,
            isAvailable: true,
            image: "",
            availableTime: [
              {
                start: "9:00",
                end: "10:30",
              },
              {
                start: "10:45",
                end: "12:15",
              },
              {
                start: "12:30",
                end: "02:00",
              },
            ],
            availableDate: [
              "2022-06-05T11:27:12.141Z",
              "2022-06-06T11:27:12.141Z",
              "2022-06-07T11:27:12.141Z",
            ],
          },
          {
            name: "Person 3",
            price: 30,
            isAvailable: true,
            image: "",
            availableTime: [
              {
                start: "9:00",
                end: "10:30",
              },
              {
                start: "10:45",
                end: "12:15",
              },
              {
                start: "12:30",
                end: "02:00",
              },
            ],
            availableDate: [
              "2022-06-05T11:27:12.141Z",
              "2022-06-06T11:27:12.141Z",
              "2022-06-07T11:27:12.141Z",
            ],
          },
          {
            name: "Person 4",
            price: 40,
            isAvailable: true,
            image: "",
            availableTime: [
              {
                start: "9:00",
                end: "10:30",
              },
              {
                start: "10:45",
                end: "12:15",
              },
              {
                start: "12:30",
                end: "02:00",
              },
            ],
            availableDate: [
              "2022-06-05T11:27:12.141Z",
              "2022-06-06T11:27:12.141Z",
              "2022-06-07T11:27:12.141Z",
            ],
          },
          {
            name: "Person 5",
            price: 50,
            isAvailable: true,
            image: "",
            availableTime: [
              {
                start: "9:00",
                end: "10:30",
              },
              {
                start: "10:45",
                end: "12:15",
              },
              {
                start: "12:30",
                end: "02:00",
              },
            ],
            availableDate: [
              "2022-06-05T11:27:12.141Z",
              "2022-06-06T11:27:12.141Z",
              "2022-06-07T11:27:12.141Z",
            ],
          },
        ],
      },
      {
        date: "2022-06-08T11:27:12.141Z",
        availablePeople: [
          {
            name: "Person 1",
            price: 10,
            isAvailable: true,
            image: "",
            availableTime: [
              {
                start: "9:00",
                end: "10:30",
              },
              {
                start: "10:45",
                end: "12:15",
              },
              {
                start: "12:30",
                end: "02:00",
              },
            ],
            availableDate: [
              "2022-06-05T11:27:12.141Z",
              "2022-06-06T11:27:12.141Z",
              "2022-06-07T11:27:12.141Z",
            ],
          },
          {
            name: "Person 2",
            price: 20,
            isAvailable: true,
            image: "",
            availableTime: [
              {
                start: "9:00",
                end: "10:30",
              },
              {
                start: "10:45",
                end: "12:15",
              },
              {
                start: "12:30",
                end: "02:00",
              },
            ],
            availableDate: [
              "2022-06-05T11:27:12.141Z",
              "2022-06-06T11:27:12.141Z",
              "2022-06-07T11:27:12.141Z",
            ],
          },
          {
            name: "Person 3",
            price: 30,
            isAvailable: true,
            image: "",
            availableTime: [
              {
                start: "9:00",
                end: "10:30",
              },
              {
                start: "10:45",
                end: "12:15",
              },
              {
                start: "12:30",
                end: "02:00",
              },
            ],
            availableDate: [
              "2022-06-05T11:27:12.141Z",
              "2022-06-06T11:27:12.141Z",
              "2022-06-07T11:27:12.141Z",
            ],
          },
          {
            name: "Person 4",
            price: 40,
            isAvailable: true,
            image: "",
            availableTime: [
              {
                start: "9:00",
                end: "10:30",
              },
              {
                start: "10:45",
                end: "12:15",
              },
              {
                start: "12:30",
                end: "02:00",
              },
            ],
            availableDate: [
              "2022-06-05T11:27:12.141Z",
              "2022-06-06T11:27:12.141Z",
              "2022-06-07T11:27:12.141Z",
            ],
          },
          {
            name: "Person 5",
            price: 50,
            isAvailable: true,
            image: "",
            availableTime: [
              {
                start: "9:00",
                end: "10:30",
              },
              {
                start: "10:45",
                end: "12:15",
              },
              {
                start: "12:30",
                end: "02:00",
              },
            ],
            availableDate: [
              "2022-06-05T11:27:12.141Z",
              "2022-06-06T11:27:12.141Z",
              "2022-06-07T11:27:12.141Z",
            ],
          },
        ],
      },
      {
        date: "2022-06-09T11:27:12.141Z",
        availablePeople: [
          {
            name: "Person 1",
            price: 10,
            isAvailable: true,
            image: "",
            availableTime: [
              {
                start: "9:00",
                end: "10:30",
              },
              {
                start: "10:45",
                end: "12:15",
              },
              {
                start: "12:30",
                end: "02:00",
              },
            ],
            availableDate: [
              "2022-06-05T11:27:12.141Z",
              "2022-06-06T11:27:12.141Z",
              "2022-06-07T11:27:12.141Z",
            ],
          },
          {
            name: "Person 2",
            price: 20,
            isAvailable: true,
            image: "",
            availableTime: [
              {
                start: "9:00",
                end: "10:30",
              },
              {
                start: "10:45",
                end: "12:15",
              },
              {
                start: "12:30",
                end: "02:00",
              },
            ],
            availableDate: [
              "2022-06-05T11:27:12.141Z",
              "2022-06-06T11:27:12.141Z",
              "2022-06-07T11:27:12.141Z",
            ],
          },
          {
            name: "Person 3",
            price: 30,
            isAvailable: true,
            image: "",
            availableTime: [
              {
                start: "9:00",
                end: "10:30",
              },
              {
                start: "10:45",
                end: "12:15",
              },
              {
                start: "12:30",
                end: "02:00",
              },
            ],
            availableDate: [
              "2022-06-05T11:27:12.141Z",
              "2022-06-06T11:27:12.141Z",
              "2022-06-07T11:27:12.141Z",
            ],
          },
          {
            name: "Person 4",
            price: 40,
            isAvailable: true,
            image: "",
            availableTime: [
              {
                start: "9:00",
                end: "10:30",
              },
              {
                start: "10:45",
                end: "12:15",
              },
              {
                start: "12:30",
                end: "02:00",
              },
            ],
            availableDate: [
              "2022-06-05T11:27:12.141Z",
              "2022-06-06T11:27:12.141Z",
              "2022-06-07T11:27:12.141Z",
            ],
          },
          {
            name: "Person 5",
            price: 50,
            isAvailable: true,
            image: "",
            availableTime: [
              {
                start: "9:00",
                end: "10:30",
              },
              {
                start: "10:45",
                end: "12:15",
              },
              {
                start: "12:30",
                end: "02:00",
              },
            ],
            availableDate: [
              "2022-06-05T11:27:12.141Z",
              "2022-06-06T11:27:12.141Z",
              "2022-06-07T11:27:12.141Z",
            ],
          },
        ],
      },
      {
        date: "2022-06-10T11:27:12.141Z",
        availablePeople: [
          {
            name: "Person 1",
            price: 10,
            isAvailable: true,
            image: "",
            availableTime: [
              {
                start: "9:00",
                end: "10:30",
              },
              {
                start: "10:45",
                end: "12:15",
              },
              {
                start: "12:30",
                end: "02:00",
              },
            ],
            availableDate: [
              "2022-06-05T11:27:12.141Z",
              "2022-06-06T11:27:12.141Z",
              "2022-06-07T11:27:12.141Z",
            ],
          },
          {
            name: "Person 2",
            price: 20,
            isAvailable: true,
            image: "",
            availableTime: [
              {
                start: "9:00",
                end: "10:30",
              },
              {
                start: "10:45",
                end: "12:15",
              },
              {
                start: "12:30",
                end: "02:00",
              },
            ],
            availableDate: [
              "2022-06-05T11:27:12.141Z",
              "2022-06-06T11:27:12.141Z",
              "2022-06-07T11:27:12.141Z",
            ],
          },
          {
            name: "Person 3",
            price: 30,
            isAvailable: true,
            image: "",
            availableTime: [
              {
                start: "9:00",
                end: "10:30",
              },
              {
                start: "10:45",
                end: "12:15",
              },
              {
                start: "12:30",
                end: "02:00",
              },
            ],
            availableDate: [
              "2022-06-05T11:27:12.141Z",
              "2022-06-06T11:27:12.141Z",
              "2022-06-07T11:27:12.141Z",
            ],
          },
          {
            name: "Person 4",
            price: 40,
            isAvailable: true,
            image: "",
            availableTime: [
              {
                start: "9:00",
                end: "10:30",
              },
              {
                start: "10:45",
                end: "12:15",
              },
              {
                start: "12:30",
                end: "02:00",
              },
            ],
            availableDate: [
              "2022-06-05T11:27:12.141Z",
              "2022-06-06T11:27:12.141Z",
              "2022-06-07T11:27:12.141Z",
            ],
          },
          {
            name: "Person 5",
            price: 50,
            isAvailable: true,
            image: "",
            availableTime: [
              {
                start: "9:00",
                end: "10:30",
              },
              {
                start: "10:45",
                end: "12:15",
              },
              {
                start: "12:30",
                end: "02:00",
              },
            ],
            availableDate: [
              "2022-06-05T11:27:12.141Z",
              "2022-06-06T11:27:12.141Z",
              "2022-06-07T11:27:12.141Z",
            ],
          },
        ],
      },
      {
        date: "2022-06-11T11:27:12.141Z",
        availablePeople: [
          {
            name: "Person 1",
            price: 10,
            isAvailable: true,
            image: "",
            availableTime: [
              {
                start: "9:00",
                end: "10:30",
              },
              {
                start: "10:45",
                end: "12:15",
              },
              {
                start: "12:30",
                end: "02:00",
              },
            ],
            availableDate: [
              "2022-06-05T11:27:12.141Z",
              "2022-06-06T11:27:12.141Z",
              "2022-06-07T11:27:12.141Z",
            ],
          },
          {
            name: "Person 2",
            price: 20,
            isAvailable: true,
            image: "",
            availableTime: [
              {
                start: "9:00",
                end: "10:30",
              },
              {
                start: "10:45",
                end: "12:15",
              },
              {
                start: "12:30",
                end: "02:00",
              },
            ],
            availableDate: [
              "2022-06-05T11:27:12.141Z",
              "2022-06-06T11:27:12.141Z",
              "2022-06-07T11:27:12.141Z",
            ],
          },
          {
            name: "Person 3",
            price: 30,
            isAvailable: true,
            image: "",
            availableTime: [
              {
                start: "9:00",
                end: "10:30",
              },
              {
                start: "10:45",
                end: "12:15",
              },
              {
                start: "12:30",
                end: "02:00",
              },
            ],
            availableDate: [
              "2022-06-05T11:27:12.141Z",
              "2022-06-06T11:27:12.141Z",
              "2022-06-07T11:27:12.141Z",
            ],
          },
          {
            name: "Person 4",
            price: 40,
            isAvailable: true,
            image: "",
            availableTime: [
              {
                start: "9:00",
                end: "10:30",
              },
              {
                start: "10:45",
                end: "12:15",
              },
              {
                start: "12:30",
                end: "02:00",
              },
            ],
            availableDate: [
              "2022-06-05T11:27:12.141Z",
              "2022-06-06T11:27:12.141Z",
              "2022-06-07T11:27:12.141Z",
            ],
          },
          {
            name: "Person 5",
            price: 50,
            isAvailable: true,
            image: "",
            availableTime: [
              {
                start: "9:00",
                end: "10:30",
              },
              {
                start: "10:45",
                end: "12:15",
              },
              {
                start: "12:30",
                end: "02:00",
              },
            ],
            availableDate: [
              "2022-06-05T11:27:12.141Z",
              "2022-06-06T11:27:12.141Z",
              "2022-06-07T11:27:12.141Z",
            ],
          },
        ],
      },
    ],
  };
  //   availablePeople: [
  //     {
  //       name: "Person 1",
  //       price: 10,
  //       isAvailable: true,
  //       image: "",
  //       availableTime: [
  //         {
  //           start: "9:00",
  //           end: "10:30",
  //         },
  //         {
  //           start: "10:45",
  //           end: "12:15",
  //         },
  //         {
  //           start: "12:30",
  //           end: "02:00",
  //         },
  //       ],
  //       availableDate: [
  //         "2022-06-05T11:27:12.141Z",
  //         "2022-06-06T11:27:12.141Z",
  //         "2022-06-07T11:27:12.141Z"
  //       ]
  //     },
  //     {
  //       name: "Person 2",
  //       price: 20,
  //       isAvailable: true,
  //       image: "",
  //       availableTime: [
  //         {
  //           start: "9:00",
  //           end: "10:30",
  //         },
  //         {
  //           start: "10:45",
  //           end: "12:15",
  //         },
  //         {
  //           start: "12:30",
  //           end: "02:00",
  //         },
  //       ],
  //       availableDate: [
  //         "2022-06-05T11:27:12.141Z",
  //         "2022-06-06T11:27:12.141Z",
  //         "2022-06-07T11:27:12.141Z"
  //       ]
  //     },
  //     {
  //       name: "Person 3",
  //       price: 30,
  //       isAvailable: true,
  //       image: "",
  //       availableTime: [
  //         {
  //           start: "9:00",
  //           end: "10:30",
  //         },
  //         {
  //           start: "10:45",
  //           end: "12:15",
  //         },
  //         {
  //           start: "12:30",
  //           end: "02:00",
  //         },
  //       ],
  //       availableDate: [
  //         "2022-06-05T11:27:12.141Z",
  //         "2022-06-06T11:27:12.141Z",
  //         "2022-06-07T11:27:12.141Z"
  //       ]
  //     },
  //     {
  //       name: "Person 4",
  //       price: 40,
  //       isAvailable: true,
  //       image: "",
  //       availableTime: [
  //         {
  //           start: "9:00",
  //           end: "10:30",
  //         },
  //         {
  //           start: "10:45",
  //           end: "12:15",
  //         },
  //         {
  //           start: "12:30",
  //           end: "02:00",
  //         },
  //       ],
  //       availableDate: [
  //         "2022-06-05T11:27:12.141Z",
  //         "2022-06-06T11:27:12.141Z",
  //         "2022-06-07T11:27:12.141Z"
  //       ]
  //     },
  //     {
  //       name: "Person 5",
  //       price: 50,
  //       isAvailable: true,
  //       image: "",
  //       availableTime: [
  //         {
  //           start: "9:00",
  //           end: "10:30",
  //         },
  //         {
  //           start: "10:45",
  //           end: "12:15",
  //         },
  //         {
  //           start: "12:30",
  //           end: "02:00",
  //         },
  //       ],
  //       availableDate: [
  //         "2022-06-05T11:27:12.141Z",
  //         "2022-06-06T11:27:12.141Z",
  //         "2022-06-07T11:27:12.141Z"
  //       ]
  //     },
  //   ],
  // },
  const business2: BusinessDetails = {
    name: "Business 2 Name",
    address: "Business 2 Location",
    phone: "416-123-4567",
    website: "www.business2.com",
    service: {
      name: "Service 2",
      description: "Service 2 description",
      price: 100,
      duration: 60,
      currency: "$",
      options: [
        {
          label: "Option 1",
          values: [
            { label: "Value 1", price: 10 },
            { label: "Value 2", price: 20 },
            { label: "Value 3", price: 30 },
          ],
        },
        {
          label: "Option 2",
          values: [
            { label: "Value 1", price: 10 },
            { label: "Value 2", price: 20 },
            { label: "Value 3", price: 30 },
          ],
        },
        {
          label: "Option 3",
          values: [
            { label: "Value 1", price: 10 },
            { label: "Value 2", price: 20 },
            { label: "Value 3", price: 30 },
          ],
        },
      ],
    },
    availableDates: [
      {
        date: "2022-06-06T11:27:12.141Z",
        availablePeople: [
          {
            name: "Person 1",
            price: 10,
            isAvailable: true,
            image: "",
            availableTime: [
              {
                start: "9:00",
                end: "10:30",
              },
              {
                start: "10:45",
                end: "12:15",
              },
              {
                start: "12:30",
                end: "02:00",
              },
            ],
            availableDate: [
              "2022-06-05T11:27:12.141Z",
              "2022-06-06T11:27:12.141Z",
              "2022-06-07T11:27:12.141Z",
            ],
          },
          {
            name: "Person 2",
            price: 20,
            isAvailable: true,
            image: "",
            availableTime: [
              {
                start: "9:00",
                end: "10:30",
              },
              {
                start: "10:45",
                end: "12:15",
              },
              {
                start: "12:30",
                end: "02:00",
              },
            ],
            availableDate: [
              "2022-06-05T11:27:12.141Z",
              "2022-06-06T11:27:12.141Z",
              "2022-06-07T11:27:12.141Z",
            ],
          },
          {
            name: "Person 3",
            price: 30,
            isAvailable: true,
            image: "",
            availableTime: [
              {
                start: "9:00",
                end: "10:30",
              },
              {
                start: "10:45",
                end: "12:15",
              },
              {
                start: "12:30",
                end: "02:00",
              },
            ],
            availableDate: [
              "2022-06-05T11:27:12.141Z",
              "2022-06-06T11:27:12.141Z",
              "2022-06-07T11:27:12.141Z",
            ],
          },
          {
            name: "Person 4",
            price: 40,
            isAvailable: true,
            image: "",
            availableTime: [
              {
                start: "9:00",
                end: "10:30",
              },
              {
                start: "10:45",
                end: "12:15",
              },
              {
                start: "12:30",
                end: "02:00",
              },
            ],
            availableDate: [
              "2022-06-05T11:27:12.141Z",
              "2022-06-06T11:27:12.141Z",
              "2022-06-07T11:27:12.141Z",
            ],
          },
          {
            name: "Person 5",
            price: 50,
            isAvailable: true,
            image: "",
            availableTime: [
              {
                start: "9:00",
                end: "10:30",
              },
              {
                start: "10:45",
                end: "12:15",
              },
              {
                start: "12:30",
                end: "02:00",
              },
            ],
            availableDate: [
              "2022-06-05T11:27:12.141Z",
              "2022-06-06T11:27:12.141Z",
              "2022-06-07T11:27:12.141Z",
            ],
          },
        ],
      },
      {
        date: "2022-06-07T11:27:12.141Z",
        availablePeople: [
          {
            name: "Person 1",
            price: 10,
            isAvailable: true,
            image: "",
            availableTime: [
              {
                start: "9:00",
                end: "10:30",
              },
              {
                start: "10:45",
                end: "12:15",
              },
              {
                start: "12:30",
                end: "02:00",
              },
            ],
            availableDate: [
              "2022-06-05T11:27:12.141Z",
              "2022-06-06T11:27:12.141Z",
              "2022-06-07T11:27:12.141Z",
            ],
          },
          {
            name: "Person 2",
            price: 20,
            isAvailable: true,
            image: "",
            availableTime: [
              {
                start: "9:00",
                end: "10:30",
              },
              {
                start: "10:45",
                end: "12:15",
              },
              {
                start: "12:30",
                end: "02:00",
              },
            ],
            availableDate: [
              "2022-06-05T11:27:12.141Z",
              "2022-06-06T11:27:12.141Z",
              "2022-06-07T11:27:12.141Z",
            ],
          },
          {
            name: "Person 3",
            price: 30,
            isAvailable: true,
            image: "",
            availableTime: [
              {
                start: "9:00",
                end: "10:30",
              },
              {
                start: "10:45",
                end: "12:15",
              },
              {
                start: "12:30",
                end: "02:00",
              },
            ],
            availableDate: [
              "2022-06-05T11:27:12.141Z",
              "2022-06-06T11:27:12.141Z",
              "2022-06-07T11:27:12.141Z",
            ],
          },
          {
            name: "Person 4",
            price: 40,
            isAvailable: true,
            image: "",
            availableTime: [
              {
                start: "9:00",
                end: "10:30",
              },
              {
                start: "10:45",
                end: "12:15",
              },
              {
                start: "12:30",
                end: "02:00",
              },
            ],
            availableDate: [
              "2022-06-05T11:27:12.141Z",
              "2022-06-06T11:27:12.141Z",
              "2022-06-07T11:27:12.141Z",
            ],
          },
          {
            name: "Person 5",
            price: 50,
            isAvailable: true,
            image: "",
            availableTime: [
              {
                start: "9:00",
                end: "10:30",
              },
              {
                start: "10:45",
                end: "12:15",
              },
              {
                start: "12:30",
                end: "02:00",
              },
            ],
            availableDate: [
              "2022-06-05T11:27:12.141Z",
              "2022-06-06T11:27:12.141Z",
              "2022-06-07T11:27:12.141Z",
            ],
          },
        ],
      },
      {
        date: "2022-06-08T11:27:12.141Z",
        availablePeople: [
          {
            name: "Person 1",
            price: 10,
            isAvailable: true,
            image: "",
            availableTime: [
              {
                start: "9:00",
                end: "10:30",
              },
              {
                start: "10:45",
                end: "12:15",
              },
              {
                start: "12:30",
                end: "02:00",
              },
            ],
            availableDate: [
              "2022-06-05T11:27:12.141Z",
              "2022-06-06T11:27:12.141Z",
              "2022-06-07T11:27:12.141Z",
            ],
          },
          {
            name: "Person 2",
            price: 20,
            isAvailable: true,
            image: "",
            availableTime: [
              {
                start: "9:00",
                end: "10:30",
              },
              {
                start: "10:45",
                end: "12:15",
              },
              {
                start: "12:30",
                end: "02:00",
              },
            ],
            availableDate: [
              "2022-06-05T11:27:12.141Z",
              "2022-06-06T11:27:12.141Z",
              "2022-06-07T11:27:12.141Z",
            ],
          },
          {
            name: "Person 3",
            price: 30,
            isAvailable: true,
            image: "",
            availableTime: [
              {
                start: "9:00",
                end: "10:30",
              },
              {
                start: "10:45",
                end: "12:15",
              },
              {
                start: "12:30",
                end: "02:00",
              },
            ],
            availableDate: [
              "2022-06-05T11:27:12.141Z",
              "2022-06-06T11:27:12.141Z",
              "2022-06-07T11:27:12.141Z",
            ],
          },
          {
            name: "Person 4",
            price: 40,
            isAvailable: true,
            image: "",
            availableTime: [
              {
                start: "9:00",
                end: "10:30",
              },
              {
                start: "10:45",
                end: "12:15",
              },
              {
                start: "12:30",
                end: "02:00",
              },
            ],
            availableDate: [
              "2022-06-05T11:27:12.141Z",
              "2022-06-06T11:27:12.141Z",
              "2022-06-07T11:27:12.141Z",
            ],
          },
          {
            name: "Person 5",
            price: 50,
            isAvailable: true,
            image: "",
            availableTime: [
              {
                start: "9:00",
                end: "10:30",
              },
              {
                start: "10:45",
                end: "12:15",
              },
              {
                start: "12:30",
                end: "02:00",
              },
            ],
            availableDate: [
              "2022-06-05T11:27:12.141Z",
              "2022-06-06T11:27:12.141Z",
              "2022-06-07T11:27:12.141Z",
            ],
          },
        ],
      },
      {
        date: "2022-06-09T11:27:12.141Z",
        availablePeople: [
          {
            name: "Person 1",
            price: 10,
            isAvailable: true,
            image: "",
            availableTime: [
              {
                start: "9:00",
                end: "10:30",
              },
              {
                start: "10:45",
                end: "12:15",
              },
              {
                start: "12:30",
                end: "02:00",
              },
            ],
            availableDate: [
              "2022-06-05T11:27:12.141Z",
              "2022-06-06T11:27:12.141Z",
              "2022-06-07T11:27:12.141Z",
            ],
          },
          {
            name: "Person 2",
            price: 20,
            isAvailable: true,
            image: "",
            availableTime: [
              {
                start: "9:00",
                end: "10:30",
              },
              {
                start: "10:45",
                end: "12:15",
              },
              {
                start: "12:30",
                end: "02:00",
              },
            ],
            availableDate: [
              "2022-06-05T11:27:12.141Z",
              "2022-06-06T11:27:12.141Z",
              "2022-06-07T11:27:12.141Z",
            ],
          },
          {
            name: "Person 3",
            price: 30,
            isAvailable: true,
            image: "",
            availableTime: [
              {
                start: "9:00",
                end: "10:30",
              },
              {
                start: "10:45",
                end: "12:15",
              },
              {
                start: "12:30",
                end: "02:00",
              },
            ],
            availableDate: [
              "2022-06-05T11:27:12.141Z",
              "2022-06-06T11:27:12.141Z",
              "2022-06-07T11:27:12.141Z",
            ],
          },
          {
            name: "Person 4",
            price: 40,
            isAvailable: true,
            image: "",
            availableTime: [
              {
                start: "9:00",
                end: "10:30",
              },
              {
                start: "10:45",
                end: "12:15",
              },
              {
                start: "12:30",
                end: "02:00",
              },
            ],
            availableDate: [
              "2022-06-05T11:27:12.141Z",
              "2022-06-06T11:27:12.141Z",
              "2022-06-07T11:27:12.141Z",
            ],
          },
          {
            name: "Person 5",
            price: 50,
            isAvailable: true,
            image: "",
            availableTime: [
              {
                start: "9:00",
                end: "10:30",
              },
              {
                start: "10:45",
                end: "12:15",
              },
              {
                start: "12:30",
                end: "02:00",
              },
            ],
            availableDate: [
              "2022-06-05T11:27:12.141Z",
              "2022-06-06T11:27:12.141Z",
              "2022-06-07T11:27:12.141Z",
            ],
          },
        ],
      },
      {
        date: "2022-06-10T11:27:12.141Z",
        availablePeople: [
          {
            name: "Person 1",
            price: 10,
            isAvailable: true,
            image: "",
            availableTime: [
              {
                start: "9:00",
                end: "10:30",
              },
              {
                start: "10:45",
                end: "12:15",
              },
              {
                start: "12:30",
                end: "02:00",
              },
            ],
            availableDate: [
              "2022-06-05T11:27:12.141Z",
              "2022-06-06T11:27:12.141Z",
              "2022-06-07T11:27:12.141Z",
            ],
          },
          {
            name: "Person 2",
            price: 20,
            isAvailable: true,
            image: "",
            availableTime: [
              {
                start: "9:00",
                end: "10:30",
              },
              {
                start: "10:45",
                end: "12:15",
              },
              {
                start: "12:30",
                end: "02:00",
              },
            ],
            availableDate: [
              "2022-06-05T11:27:12.141Z",
              "2022-06-06T11:27:12.141Z",
              "2022-06-07T11:27:12.141Z",
            ],
          },
          {
            name: "Person 3",
            price: 30,
            isAvailable: true,
            image: "",
            availableTime: [
              {
                start: "9:00",
                end: "10:30",
              },
              {
                start: "10:45",
                end: "12:15",
              },
              {
                start: "12:30",
                end: "02:00",
              },
            ],
            availableDate: [
              "2022-06-05T11:27:12.141Z",
              "2022-06-06T11:27:12.141Z",
              "2022-06-07T11:27:12.141Z",
            ],
          },
          {
            name: "Person 4",
            price: 40,
            isAvailable: true,
            image: "",
            availableTime: [
              {
                start: "9:00",
                end: "10:30",
              },
              {
                start: "10:45",
                end: "12:15",
              },
              {
                start: "12:30",
                end: "02:00",
              },
            ],
            availableDate: [
              "2022-06-05T11:27:12.141Z",
              "2022-06-06T11:27:12.141Z",
              "2022-06-07T11:27:12.141Z",
            ],
          },
          {
            name: "Person 5",
            price: 50,
            isAvailable: true,
            image: "",
            availableTime: [
              {
                start: "9:00",
                end: "10:30",
              },
              {
                start: "10:45",
                end: "12:15",
              },
              {
                start: "12:30",
                end: "02:00",
              },
            ],
            availableDate: [
              "2022-06-05T11:27:12.141Z",
              "2022-06-06T11:27:12.141Z",
              "2022-06-07T11:27:12.141Z",
            ],
          },
        ],
      },
      {
        date: "2022-06-11T11:27:12.141Z",
        availablePeople: [
          {
            name: "Person 1",
            price: 10,
            isAvailable: true,
            image: "",
            availableTime: [
              {
                start: "9:00",
                end: "10:30",
              },
              {
                start: "10:45",
                end: "12:15",
              },
              {
                start: "12:30",
                end: "02:00",
              },
            ],
            availableDate: [
              "2022-06-05T11:27:12.141Z",
              "2022-06-06T11:27:12.141Z",
              "2022-06-07T11:27:12.141Z",
            ],
          },
          {
            name: "Person 2",
            price: 20,
            isAvailable: true,
            image: "",
            availableTime: [
              {
                start: "9:00",
                end: "10:30",
              },
              {
                start: "10:45",
                end: "12:15",
              },
              {
                start: "12:30",
                end: "02:00",
              },
            ],
            availableDate: [
              "2022-06-05T11:27:12.141Z",
              "2022-06-06T11:27:12.141Z",
              "2022-06-07T11:27:12.141Z",
            ],
          },
          {
            name: "Person 3",
            price: 30,
            isAvailable: true,
            image: "",
            availableTime: [
              {
                start: "9:00",
                end: "10:30",
              },
              {
                start: "10:45",
                end: "12:15",
              },
              {
                start: "12:30",
                end: "02:00",
              },
            ],
            availableDate: [
              "2022-06-05T11:27:12.141Z",
              "2022-06-06T11:27:12.141Z",
              "2022-06-07T11:27:12.141Z",
            ],
          },
          {
            name: "Person 4",
            price: 40,
            isAvailable: true,
            image: "",
            availableTime: [
              {
                start: "9:00",
                end: "10:30",
              },
              {
                start: "10:45",
                end: "12:15",
              },
              {
                start: "12:30",
                end: "02:00",
              },
            ],
            availableDate: [
              "2022-06-05T11:27:12.141Z",
              "2022-06-06T11:27:12.141Z",
              "2022-06-07T11:27:12.141Z",
            ],
          },
          {
            name: "Person 5",
            price: 50,
            isAvailable: true,
            image: "",
            availableTime: [
              {
                start: "9:00",
                end: "10:30",
              },
              {
                start: "10:45",
                end: "12:15",
              },
              {
                start: "12:30",
                end: "02:00",
              },
            ],
            availableDate: [
              "2022-06-05T11:27:12.141Z",
              "2022-06-06T11:27:12.141Z",
              "2022-06-07T11:27:12.141Z",
            ],
          },
        ],
      },
    ],
  };
  const serviceDetails: BusinessDetails = businessKey ? business1 : business2;

  const [service, setService] = React.useState<SelectedServiceOptions>({});

  const handleSetService = (option: { name: any; value: any }) => {
    setService((prevState) => {
      return {
        ...prevState,
        [option.name]: option.value,
      };
    });
  };

  function handleModalClose(): void {
    setActiveStep("customize");
    setService({});
    handleClose();
  }

  const handleStep = (step: ServiceSteps) => {
    setActiveStep(step);
  };
  const stepMapper: Record<ServiceSteps, number> = {
    customize: 0,
    date: 1,
    time: 2,
    extras: 3,
    confirmed: 4,
  };

  return (
    <Dialog
      open={open}
      onClose={handleModalClose}
      fullWidth
    >
      <ContentBlock>
        <Grid
          container
          direction="row"
          justifyContent="flex-start"
          alignItems="flex-start"
          spacing={0}
          sx={{
            padding: "0px",
          }}
        >
          <Grid
            item
            md={
              step === "customize" || !serviceDetails.availableDates.length
                ? 12
                : 7
            }
            padding={"10px"}
          >
            <Stack
              gap={3}
              direction="column"
              justifyContent="center"
              alignItems="center"
              spacing={2}
            >
              <div
                style={{
                  padding: "15px",
                }}
              >
                {step === "customize" ? (
                  <CustomizeOption
                    key={"customize"}
                    selectedOption={service}
                    handleStepChange={handleStep}
                    handleSetSelectedOption={handleSetService}
                    service={serviceDetails}
                  />
                ) : step === "date" ? (
                  <DateOption
                    key={"date"}
                    selectedOption={service}
                    handleStepChange={handleStep}
                    handleSetSelectedOption={handleSetService}
                    service={serviceDetails}
                  />
                ) : step === "time" ? (
                  <TimeOption
                    key={"time"}
                    selectedOption={service}
                    handleStepChange={handleStep}
                    handleSetSelectedOption={handleSetService}
                    service={serviceDetails}
                  />
                ) : step === "extras" ? (
                  <ExtrasOption
                    key={"extras"}
                    selectedOption={service}
                    handleStepChange={handleStep}
                    handleSetSelectedOption={handleSetService}
                    service={serviceDetails}
                  />
                ) : step === "confirmed" ? (
                  <ConfirmedOption
                    key={"confirmed"}
                    selectedOption={service}
                    handleStepChange={handleStep}
                    handleSetSelectedOption={handleSetService}
                    service={serviceDetails}
                    handleClose={handleClose}
                  />
                ) : null}
              </div>

              <CustomStepper
                variant="dots"
                steps={Object.keys(stepMapper).length}
                position="static"
                activeStep={stepMapper[step]}
                sx={{ maxWidth: 400 }}
                backButton={undefined}
                nextButton={undefined}
              />
            </Stack>
          </Grid>
          {step !== "customize" && serviceDetails.availableDates.length && (
            <Grid
              item
              md={5}
              sx={{
                padding: "0px",
                background: "black",
                color: "white",
                maxHeight: "inherit",
                height: "370px",
              }}
            >
              <OptionDetails
                selectedOption={service}
                handleStepChange={handleStep}
                handleSetSelectedOption={handleSetService}
                service={serviceDetails}
                handleClose={handleModalClose}
              />
            </Grid>
          )}
        </Grid>
      </ContentBlock>
      {/* <DialogActions
        sx={{
          justifyContent: "center",
        }}
      >
        <CustomStepper
          variant="dots"
          steps={Object.keys(stepMapper).length}
          position="static"
          activeStep={stepMapper[step]}
          sx={{ maxWidth: 400 }}
          backButton={undefined}
          nextButton={undefined}
        />
      </DialogActions> */}
    </Dialog>
  );
}

export default Servicemodal;
