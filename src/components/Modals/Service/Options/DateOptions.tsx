import React from "react";
import moment from 'moment';
import { Button, Grid, IconButton, Typography, FormGroup, Checkbox } from "@mui/material";
import { CheckCircleIcon, CheckCircleIconFilled, GoBackIcon } from "../../../../assets/svg";
import { ServiceViewProps } from "../../../../interfaces/designs";
import { BootStrapFormControlLabel } from "./TimeOption";

export default function DateServiceView(props: ServiceViewProps): JSX.Element {
  const { handleStepChange, service, handleSetSelectedOption, selectedOption} =
    props;
  const availableDates = service.availableDates.map((item) => item.date);
  React.useEffect(() => {
    const { date, person } = selectedOption;
    if( date && person && Object.keys(person).length) {
      handleStepChange("time")
    }
  }, [selectedOption])
  return (
    <Grid
      container
      sx={{
        padding: "0px",
      }}
    >
      <Grid item md={12}>
        <IconButton onClick={() => handleStepChange("customize")}>
          <GoBackIcon />
        </IconButton>
        <Typography
          component={"span"}
          sx={{
            fontSize: "10px",
            fontWeight: "bold",
            fontFamily: "Montserrat",
          }}
        >
          When would you like to come in?
        </Typography>
      </Grid>
      <Grid item md={12} padding={"10px"}>
        <FormGroup>
          {availableDates ? (
            availableDates.map((date) => {
              const label = moment(date).format('DD:MM:YYYY')
              return (
                <BootStrapFormControlLabel
                  label={label}
                  control={
                    <Checkbox
                      name={label}
                      checked={date === selectedOption.date}
                      onChange={() => handleSetSelectedOption({
                        name: "date",
                        value: date
                      })}
                      icon={<CheckCircleIcon />}
                      checkedIcon={<CheckCircleIconFilled />}
                    />
                  }
                />
              );
            })
          ) : (
            <span>Loading...</span>
          )}
        </FormGroup>
      </Grid>
    </Grid>
  );
}
