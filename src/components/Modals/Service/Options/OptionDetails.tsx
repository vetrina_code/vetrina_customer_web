import React from 'react';
import { ImageListItem, ImageListItemBar, Grid, IconButton, Typography, ImageList, Box, styled } from "@mui/material";
import { WhiteCloseIcon } from "../../../../assets/svg";
import { BusinessDetails, ServiceViewProps } from "../../../../interfaces/designs";

const CustomImageListItem = styled(ImageListItem)(() => ({
  "& .MuiImageListItem-img": {
    border: "white solid 1px",
    borderRadius: "8px;"
  },
  "& :hover": {
    cursor: "pointer",
  }
}))

const CustomImageListItemBar = styled(ImageListItemBar)(() => ({
  "& .MuiImageListItemBar-titleWrap": {
    padding: "0px",
    fontFamily: "Montserrat",
    fontWeight: 700,
    color: "black",
    background: "white",
    textAlign: "center",
    borderRadius: "3px;",
    marginTop: "2px",
  },
  "& .MuiImageListItemBar-title": {
    fontSize: "10px",
  },
}));

export default function ServiceExtras(props: ServiceViewProps): JSX.Element {
  const { service, handleClose, selectedOption } = props;
  const availableData = service.availableDates.find((availableDate) => availableDate.date === selectedOption.date)
  const availablePeople = availableData?.availablePeople;
  const [completedDetails, setCompletedDetails] = React.useState(false);

  React.useEffect(() => {
    const {date, time, person} = selectedOption;
    if (date && person && Object.keys(person).length && time && Object.keys(time).length) {
      setCompletedDetails(true)
    } else {
      setCompletedDetails(false)
    }
  }, [selectedOption])

  return (
    <Grid container padding={"10px"}>
      <Grid item md={12}>
        <IconButton
          onClick={handleClose}
          sx={{
            marginLeft: "200px",
            padding: "0px",
          }}
        >
          <WhiteCloseIcon />
        </IconButton>
      </Grid>
      <Grid item md={12}>
        <Typography
          sx={{
            fontSize: "10px",
            fontWeight: "bold",
            color: "white",
          }}
        >
          {completedDetails ? 'Appointment Details' : "Available " + service.service.name}
        </Typography>
        <Typography
          variant="overline"
          sx={{
            fontSize: "8px",
          }}
          component={"p"}
        >
          {"on the 7th December, 2021"}
        </Typography>
      </Grid>
      <Grid item md={12}>
        <ImageList
          sx={{ width: "100%", height: "100%", margin: 0 }}
          cols={3}
          rowHeight={70}
          gap={10}
        >
          {availablePeople ? (
            availablePeople.map((person, index) => (
              <Box sx={{
                  display: "flex",
                  justifyContent: "center",
                }}
                key={index}
                onClick={() => {
                  props.handleSetSelectedOption({
                    name: "person",
                    value: person,
                  })
                }
              }
              >
                <CustomImageListItem sx={{
                  width: 50
                }}>
                  <img
                    src={"/img/heroImage.png"}
                    alt={person.name}
                    loading="lazy"
                  />
                  <CustomImageListItemBar title={person.name} position="below" />
                </CustomImageListItem>

              </Box>
            ))
          ) : (
            <></>
          )}
        </ImageList>
      </Grid>
      <Grid item md={12}>
        <Box sx={{
          width: "100%",
          height: "50px",
        }}>
        </Box>
      </Grid>
    </Grid>
  );
}