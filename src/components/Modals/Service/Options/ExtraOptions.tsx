import {
  Button,
  Grid,
  IconButton,
  Input,
  TextareaAutosize,
  Typography,
} from "@mui/material";
import GoBackIcon from "../../../../assets/svg/GoBackIcon";
import { ServiceViewProps } from "../../../../interfaces/designs";

export default function ExtraServiceView(props: ServiceViewProps): JSX.Element {
  const { handleStepChange, selectedOption, handleSetSelectedOption } = props;

  return (
    <Grid container>
      <Grid item md={12}>
        <IconButton onClick={() => {
           handleSetSelectedOption({
            name: "time",
            value: '',
          });
            handleStepChange("time")
          }}>
          <GoBackIcon />
        </IconButton>
      </Grid>
      <Grid item md={12} padding={"10px"}>
        <Typography
          component={"span"}
          sx={{
            fontSize: "10px",
            fontWeight: "bold",
            fontFamily: "Montserrat",
          }}
        >
          Additional Notes
        </Typography>
      </Grid>
      <Grid item md={12} padding={"10px"}>
        <TextareaAutosize
          maxRows={10}
          aria-label="maximum height"
          placeholder="Maximum 4 rows"
          defaultValue="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
      ut labore et dolore magna aliqua."
          value={selectedOption.notes}
          style={{ width: "100%" }}
          onChange={(event) =>
            handleSetSelectedOption({
              name: "notes",
              value: event.target.value,
            })
          }
        />
      </Grid>
      <Grid>
        <Button
          onClick={() => props.handleStepChange("confirmed")}
          sx={{
            background: "rgba(37, 29, 42, 1)",
            color: "white",
            padding: "8px",
            fontFamily: "Montserrat",
            "&:hover": {
              background: "rgba(37, 29, 42, 0.8)",
            },
            fontSize: '6px'
          }}
        >
          ADD TO STACK
        </Button>
      </Grid>
    </Grid>
  );
}
