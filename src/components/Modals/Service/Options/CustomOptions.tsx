import {
  SelectChangeEvent,
  Grid,
  FormControl,
  FormHelperText,
  Select,
  MenuItem,
  Button,
} from "@mui/material";
import React from "react";
import { useAppSelector } from "../../../../app/store";
import { selectBusiness, selectService } from "../../../../features/Merchant";
import { ServiceViewProps } from "../../../../interfaces/designs";
import { BusinessServicesI, MarketPlaceBusinessI } from "../../../../interfaces/slices";
import { ModalTitleText } from "../../components";

export default function CustomizeServiceView(
  props: ServiceViewProps
): JSX.Element {
  const { service } = props;
  const [selectedOption, setSelectedOption] = React.useState<
    Record<string, string | number>
  >({});

  const handleSelectedOptionChange = (
    event: SelectChangeEvent<string | number>
  ): void => {
    const { name, value } = event.target;
    setSelectedOption((prevState) => {
      const newState = { ...prevState, [name]: value };
      return newState;
    });
  };
  const new_service_details = useAppSelector(selectService) as BusinessServicesI;
  const businessDetails = useAppSelector(selectBusiness) as MarketPlaceBusinessI;
  return (
    <>
      <ModalTitleText>Customize Your View</ModalTitleText>
      <Grid container margin={"10px 0px"}>
        <Grid item md={4} sx={{}}>
          <img
            src="/img/heroImage.png"
            alt="hero"
            width="100%"
            height="150px"
            style={{
              borderRadius: "10px",
              marginBottom: "1rem",
            }}
          />
          {new_service_details && (
            <div
              style={{
                fontWeight: 600,
                fontSize: "12px",
              }}
            >
              <p>{businessDetails.business_name}</p>
              <p>{new_service_details.name}</p>
              <p>
                <span>$</span>
                {new_service_details.price}
              </p>
            </div>
          )}
        </Grid>
        <Grid item md={4}>
          {Object.keys(new_service_details).length ?
            new_service_details.customizations.map((option, index) => {
              return (
                <FormControl
                  key={index}
                  fullWidth
                  sx={{
                    padding: "5px",
                    fontWeight: 600,
                    fontSize: "12px",
                  }}
                >
                  <FormHelperText
                    sx={{
                      fontWeight: 700,
                    }}
                  >
                    {option.name}
                  </FormHelperText>
                  <Select
                  sx={{
                    maxWidth: "140px",
                    height: '38px',
                    fontSize: '9px',
                    background: 'rgba(249, 249, 249, 1)',
                  }}
                    name={option.name}
                    value={
                      selectedOption[option.name]
                        ? selectedOption[option.name]
                        : option.values[0]
                    }
                    // value={selectedOption[index] ? selectedOption[index].price : 0}
                    onChange={(e) => handleSelectedOptionChange(e)}
                  >
                    {option.values.map((value, index) => {
                      return (
                        <MenuItem key={index} value={value}>
                          {value}
                        </MenuItem>
                      );
                    })}
                  </Select>
                </FormControl>
              );
            }): null}
        </Grid>
        <Grid
          container
          direction="row"
          justifyContent="flex-end"
          alignItems="flex-end"
          item
          md={4}
          sx={{ float: "right", padding: "10px" }}
        >
          <Button
            onClick={() => props.handleStepChange("date")}
            sx={{
              background: "rgba(37, 29, 42, 1)",
              color: "white",
              padding: "8px",
              fontFamily: "Montserrat",
              "&:hover": {
                background: "rgba(37, 29, 42, 0.8)",
              },
            }}
          >
            Proceed
          </Button>
        </Grid>
      </Grid>
    </>
  );
}
