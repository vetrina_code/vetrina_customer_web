import React from "react";
import {
  Button,
  Checkbox,
  FormControlLabel,
  FormGroup,
  Grid,
  IconButton,
  styled,
  Typography,
} from "@mui/material";
import { CheckCircleIcon, CheckCircleIconFilled } from "../../../../assets/svg";
import GoBackIcon from "../../../../assets/svg/GoBackIcon";
import { ServiceViewProps, TimeSlot } from "../../../../interfaces/designs";

export const BootStrapFormControlLabel = styled(FormControlLabel)(() => ({
  "& .MuiFormControlLabel-label": {
    fontFamily: "Montserrat",
    fontSize: "10px",
    fontWeight: 400,
    LineHeight: "12px",
  },
}));

export default function TimeServiceView(props: ServiceViewProps): JSX.Element {
  const { handleStepChange, selectedOption, handleSetSelectedOption, service } =
    props;
  const [timeOption, setTimeOption] = React.useState<TimeSlot>({});
  const availableData = service.availableDates.find(
    (availableDate) => availableDate.date === selectedOption.date
  );
  const availablePerson = availableData?.availablePeople.find(
    (person) => person.name === selectedOption.person?.name
  );

  React.useEffect(() => {
    handleSetSelectedOption({
      name: "time",
      value: timeOption,
    });
  }, [timeOption]);

  React.useEffect(() => {
    const { date, person, time } = selectedOption;
    if( date && person && Object.keys(person).length && time && Object.keys(time).length) {
      handleStepChange("extras")
    }
  }, [selectedOption])

  const isChecked = (x: TimeSlot, y: TimeSlot): boolean => {
    const xArray = Object.values(x);
    const yArray = Object.values(y);
    return (
      xArray.length === yArray.length &&
      xArray.every(function (element) {
        return yArray.includes(element);
      })
    );
  };
  return (
    <Grid container>
      <Grid item md={12}>
        <IconButton
          onClick={() => {
            handleSetSelectedOption({
              name: "date",
              value: '',
            });
            handleSetSelectedOption({
              name: "person",
              value: {},
            });
            handleStepChange("date");
          }}
        >
          <GoBackIcon />
        </IconButton>
        <Typography
          component={"span"}
          sx={{
            fontSize: "10px",
            fontWeight: "bold",
            fontFamily: "Montserrat",
          }}
        >
          7th December 2021
        </Typography>
      </Grid>
      <Grid item md={8} padding={"10px"}>
        {selectedOption.person && (
          <Typography
            component={"span"}
            sx={{
              fontSize: "10px",
              fontWeight: "bold",
              fontFamily: "Montserrat",
            }}
          >
            {`${selectedOption.person?.name}'s available hours`}
            <span>.</span>
          </Typography>
        )}
      </Grid>
      <Grid item md={4}>
        {selectedOption && (
          <Typography
            component={"span"}
            sx={{
              fontSize: "6px",
              fontFamily: "Montserrat",
              textDecoration: "underline",
              textTransform: "uppercase",
            }}
          >
            {`Contact ${selectedOption.person?.name}`}
          </Typography>
        )}
      </Grid>

      <Grid item md={12} padding={"10px"}>
        <FormGroup>
          {availablePerson ? (
            availablePerson.availableTime.map((timeSlot) => {
              const label = `${timeSlot.start} - ${timeSlot.end}`;
              return (
                <BootStrapFormControlLabel
                  label={label}
                  control={
                    <Checkbox
                      name={label}
                      checked={isChecked(timeOption, timeSlot)}
                      onChange={() => setTimeOption(timeSlot)}
                      icon={<CheckCircleIcon />}
                      checkedIcon={<CheckCircleIconFilled />}
                    />
                  }
                />
              );
            })
          ) : (
            <span>You Haven't Selected a date yet.</span>
          )}
        </FormGroup>
      </Grid>
      {/* <Grid item>
        <Button
          onClick={() => props.handleStepChange("extras")}
          sx={{
            background: "rgba(37, 29, 42, 1)",
            color: "white",
            padding: "8px",
            fontFamily: "Montserrat",
            "&:hover": {
              background: "rgba(37, 29, 42, 0.8)",
            },
          }}
        >
          Next Step
        </Button>
      </Grid> */}
    </Grid>
  );
}
