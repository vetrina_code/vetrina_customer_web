import { Button, Grid } from "@mui/material";
import { SuccessTickIcon } from "../../../../assets/svg";
import { ServiceViewProps } from "../../../../interfaces/designs";
import { ModalTitleText } from "../../components";

export default function ConfirmedServiceView(
  props: ServiceViewProps
): JSX.Element {
  const { service, handleClose } = props;
  return (
    <Grid container justifyContent={"center"} textAlign={"center"}>
      <Grid item>
        <SuccessTickIcon />
      </Grid>
      <Grid item>
        <ModalTitleText>
          {`"${service?.name}"Appointment has been added to your cart successfully`}
        </ModalTitleText>
      </Grid>
      <Grid item md={12}>
        <Grid container justifyContent={"space-evenly"} padding={"20px"}>
        <Button
          onClick={handleClose}
          sx={{
            background: "rgba(37, 29, 42, 1)",
            color: "white",
            padding: "8px",
            fontFamily: "Montserrat",
            "&:hover": {
              background: "rgba(37, 29, 42, 0.8)",
            },
          }}
        >
          GO TO MERCHNAT
        </Button>
        <Button
          onClick={handleClose}
          sx={{
            background: "rgba(37, 29, 42, 1)",
            color: "white",
            padding: "8px",
            fontFamily: "Montserrat",
            "&:hover": {
              background: "rgba(37, 29, 42, 0.8)",
            },
          }}
        >
          CHECKOUT
        </Button>
        </Grid>
      </Grid>
    </Grid>
  );
}
