import React from 'react';
import { Box, Button, Dialog, DialogContent, DialogTitle, Rating, styled, TextField } from '@mui/material';
import { ModalTitleText } from './components';


function getCurrentShop(): string {
  return "The Common Room"
}
function RatingComponent(): JSX.Element {
  const [value, setValue] = React.useState<number | null>(0);

  const StyledRating = styled(Rating)({
    '& .MuiRating-iconFilled': {
      color: '#ff6d75',
    },
    '& .MuiRating-iconHover': {
      color: '#ff3d47',
    },
  });
  return (
    <StyledRating
        name="customized-color"
        defaultValue={0}
        value={value}
        getLabelText={(value: number) => `${value} Heart${value !== 1 ? 's' : ''}`}
        precision={0.5}
        onChange={(event, newValue) => setValue(newValue) }
        // icon={<FavoriteIcon fontSize="inherit" />}
        // emptyIcon={<FavoriteBorderIcon fontSize="inherit" />}
      />
  )
}

function ReviewModal({open, handleClose} : {open: boolean, handleClose: () => void}): JSX.Element {
  return (
    <Dialog open={open} fullWidth onClose={handleClose}>
      <DialogTitle>
        <ModalTitleText>
          {`Review: ${getCurrentShop()}`}
        </ModalTitleText>
      </DialogTitle>
      <DialogContent>
        <RatingComponent />
        <Box
        sx={{
          width: 600,
          maxWidth: '100%',
          marginTop: "20px"
        }}
        >
        <TextField fullWidth label={`What did you think about your experience with ${getCurrentShop()}?`} id="fullWidth" multiline rows={8} />
        </Box>
        <Box sx={{float: "right", padding: "10px"}}>
          <Button sx={{
            background: "rgba(37, 29, 42, 1)",
            color: "white",
            padding: "10px",
            fontFamily: "Montserrat",
          }}>
            SUBMIT REVIEW
          </Button>
        </Box>
      </DialogContent>
    </Dialog>
  )
}

export default ReviewModal