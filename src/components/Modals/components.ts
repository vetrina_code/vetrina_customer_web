// import styled from "@emotion/styled";
import { Tooltip, Typography, TooltipProps, tooltipClasses, styled } from "@mui/material";

export const ModalTitleText = styled(Typography)(() => ({
  fontFamily: "Open Sans, Monsterrat",
  fontWeight: 600,
  fontSize: "15px",
}));

export const ModalToolTip = styled(Tooltip)<TooltipProps>(({theme}) => ({
  [`& .${tooltipClasses.tooltip}`]: {
    backgroundColor: 'transparent',
    color: 'black',
  },
}));
