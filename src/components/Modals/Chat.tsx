import * as React from "react";
import {
  Card,
  Box,
  CardHeader,
  CardContent,
  Typography,
  CardActionArea,
  CardActions,
  IconButton,
} from "@mui/material";
import ExpandIcon from "../../assets/svg/ExpandIcon";
import SupportIcon from "../../assets/svg/SupportLogo";
import styled from "styled-components";
import { Input } from "../../ui/Form/Input";
import { SendIcon } from "../../assets/svg";
import { ExpandableCard } from "../Layout/Card";

export default function ChatModal({
  open,
  onClose,
}: {
  open: boolean;
  onClose: () => void;
}) {
  const [expanded, setExpanded] = React.useState(false);

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };
  const messages = [
    {
      title: "Support",
      description: "We are here to help you with any questions you have.",
    },
    {
      title: "Support",
      description:
        "Speak to the Varroe Support team \n right here and we will get back to you ASAP.",
    },
    {
      title: "Customer",
      description: "Is Varroe Free To Use?",
    },
  ];
  const ChatBox = styled(Box)<{ title: string }>(
    ({ title }) => `
      background: ${title === "Support" ? "#f5f5f5" : "rgba(92, 14, 144, 1)"};
      color: ${title === "Support" ? "black" : "white"};
      border-radius: 10px;
      padding: 10px;
      margin: 10px;
      overflow: hidden;
      max-width: calc(100% - 60%);
      float: ${title === "Support" ? "left" : "right "};
    `
  );
  return (
    <div
      style={{
        background: "transparent",
      }}
    >
      {open && (
        <ExpandableCard expand={expanded} shadow variant="outlined">
          <CardHeader
            avatar={<SupportIcon />}
            action={
              <IconButton onClick={() => handleExpandClick}>
                <ExpandIcon />
              </IconButton>
            }
            title={
              <Typography
                variant="h6"
                sx={{
                  fontSize: "20px !important",
                  fontFamily: "Montserrat",
                }}
              >
                Chat
              </Typography>
            }
            sx={{
              background: "black",
              color: "white",
              fontSize: "20px !important",
              fontFamily: "Montserrat",
            }}
          />
          <CardContent sx={{ padding: "10px" }}>
            <Card
              variant="outlined"
              sx={{ boxShadow: "0px 0px 10px rgba(0, 0, 0, 0.50)" }}
            >
              <CardContent>
                {messages.map((message, index) => {
                  return (
                    <ChatBox title={message.title} key={index}>
                      <Typography
                        sx={{
                          fontFamily: "Montserrat",
                          fontSize: "11px",
                          fontWeight: 600,
                        }}
                      >
                        {message.description}
                      </Typography>
                    </ChatBox>
                  );
                })}
              </CardContent>
              <CardActionArea>
                <CardActions>
                  <Input placeholder="Type your message here" />
                  <IconButton>
                    <SendIcon />
                  </IconButton>
                </CardActions>
              </CardActionArea>
            </Card>
          </CardContent>
        </ExpandableCard>
      )}
    </div>
  );
}
