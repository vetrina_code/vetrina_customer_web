import React from "react";
// Interface & Types
import { MarketPlaceBusinessI } from "../../interfaces/slices";
// Design & Icon
import {
  Card,
  CardContent,
  CardHeader,
  IconButton,
  Grid,
  Rating,
  Stack,
  Paper,
  Box,
} from "@mui/material";
import { PricesIcon, VerifiedIcon } from "../../assets/svg";
import { FillButton } from "../../ui/Buttons";

function BusinessCard({ business }: { business: MarketPlaceBusinessI }) {
  const {
    is_registered,
    business_name,
    logo,
    category,
    city,
    business_description,
    country,
    average_rating,
    average_price,
  } = business;


  return (
    <Card
      sx={{
        fontSize: "7px",
        borderRadius: "10px",
      }}
    >
      <CardHeader
        sx={{
          paddingBottom: 0,
        }}
        action={
          <IconButton aria-label="settings">
            {is_registered ? <VerifiedIcon /> : null}
          </IconButton>
        }
        title={
          <img src={logo} alt={business_name} width={"70px"} height={"70px"} />
        }
      />
      <CardContent
        sx={{
          paddingTop: 0,
          paddingBottom: "0px !important",
        }}
      >
        <span
          style={{
            fontFamily: "Open Sans",
            fontSize: "18px",
            fontWeight: "600",
            lineHeight: "25px",
            letterSpacing: "0em",
            textAlign: "left",
          }}
        >
          {business_name}
        </span>
        <Grid container>
          <Grid
            item
            md={6}
            sx={{
              overflow: "hidden",
            }}
          >
            <Stack spacing={2}>
              <Box>
                <Stack spacing={0.5} direction="row">
                  <Rating
                    name="small"
                    defaultValue={average_rating}
                    size="small"
                    precision={0.5}
                    readOnly
                  />
                  <span>(10 Reviews)</span>
                  <PricesIcon />
                </Stack>
              </Box>
              <Box>
                <FillButton
                  size="small"
                  sx={{
                    margin: "0 !important",
                    fontSize: 8,
                    padding: '0px 2px !important',
                    color: "white",
                  }}
                >
                  {category.name}
                </FillButton>
              </Box>
              <Paper elevation={2}>{business_description}</Paper>
            </Stack>
          </Grid>
          <Grid
            item
            md={6}
            sx={{
              padding: "1rem",
            }}
          >
            <Stack spacing={2}>
              <span>
                {city}, {country}
              </span>
              <img
                src={logo}
                alt={business_name}
                width={"100%"}
                height={"70px"}
              />
            </Stack>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
}

export default BusinessCard;
