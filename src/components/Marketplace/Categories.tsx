import React from "react";
import { useNavigate } from "react-router-dom";
// Functions & Hooks -----------------------------------------------------------
import { useGetCategoriesQuery } from "../../service/varroe/services/api";
import { useAppDispatch } from "../../app/store";
import { setCategory } from "../../features/Market";
// Design & Icon Component
import { Stack, Box } from "@mui/material";
import DefaultCategoryIcon from "../../assets/svg/DefaultCategoryIcon";
import { HelperText } from "../../ui/Text/HelperText";
// Interfaces & Types -------------------------------------------------------------
import { MarketPlaceCategoriesI } from "../../interfaces/slices";

type BoxSize = 'small' | 'medium' | 'large'

function MarketPlaceCategories({size}: {size: BoxSize}) {
  const navigator = useNavigate();
  const dispatch = useAppDispatch();
  const { data } = useGetCategoriesQuery();
  function handleCategoryClick(category: MarketPlaceCategoriesI) {
    navigator(`/marketplace?category=${category.name}`);
    dispatch(setCategory(category));
  }

  return (
    <Stack
      direction={"row"}
      spacing={4}
      // divider={<Divider orientation="vertical" flexItem />}
    >
      {data?.data.map((category, index) => (
        <Box
          key={index}
          sx={{
            background: "#F3F3F3",
            maxWidth: size === 'small' ? "75px" : size === 'medium' ? "100px" : "150px",
            width: "100%",
            height: size === 'small' ? "100px" : size === 'medium' ? "125px" : "180px",
            borderRadius: "6px",
            padding: "5px",
            alignItems: "center",
            justifyContent: "center",
            textAlign: "center",
            whiteSpace: "nowrap",
            cursor: "pointer",
          }}
          onClick={() => {
            handleCategoryClick(category)
          }}
        >
          <Box>
            <img height={'100%'} width={'100%'} src={category.icon_url} alt='http://via.placeholder.com/200'/>
          </Box>
          <Box sx={{
            marginTop: "2px",
            textAlign: "center"
            }}>
            <HelperText
              sx={{
                fontSize: size === 'small' ? "12px" : size === 'medium' ? "14px" : "16px",
                fontWeight: "600",
                textTransform: "capitalize",
                fontFamily: "Montserrat",
              }}
            >
              {category.name}
            </HelperText>
          </Box>
        </Box>
      ))}
    </Stack>
  );
}

export default MarketPlaceCategories;