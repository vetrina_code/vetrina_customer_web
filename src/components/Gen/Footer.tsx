import React from "react";
import styled from "styled-components";
import { Container, Grid, List, ListItem, ListItemText} from "@mui/material";

import { FacebookIcon, TwitterIcon, InstagramIcon, LogoIcon} from "../../assets/svg";

const StyledContainer = styled(Container)`
  width: 100vw;
  height: 457px;
  color: black !important;
`;

export const FooterCopyRightSection = styled.div`
  border-top: 1px solid #444444;
  padding-top: 15px;
  margin: 0 60px;
  display: flex;
  justify-content: space-between;
`;
export const FooterSocialsContainer = styled.div`
  width: 30px;
  height: 30px;
  margin-right: 30px;
  background: #5c0e90;
  display: flex;
  align-items: center;
  justify-content: center;
`;
function Footer() {
  return (
    <StyledContainer>
      <Grid container style={{ padding: "20px 0px" }}>
        <Grid 
          item
          md={3}
          justifyContent={"space-evenly"}
          flexDirection='column'
          display={'flex'}
          width={'40%'}
        >
          <Grid container paddingBottom={"20px"}>
            <FooterSocialsContainer>
              <TwitterIcon height={20} width={20} />
            </FooterSocialsContainer>
            <FooterSocialsContainer>
              <InstagramIcon height={20} width={20} />
            </FooterSocialsContainer>
            <FooterSocialsContainer>
              <FacebookIcon height={20} width={20} />
            </FooterSocialsContainer>
          </Grid>
          <Grid container width={"198px"} height={"86px"} bgcolor={"#251d2a"} borderRadius={"5px"} justifyContent={'center'}>
            <LogoIcon width={160} height={76} />
          </Grid>
        </Grid>
        <Grid item md={3}>
          <List>
            <ListItem>
              <ListItemText>HELPFUL LINKS</ListItemText>
            </ListItem>
            <ListItem>About Us</ListItem>
            <ListItem>Contact Us</ListItem>
            <ListItem>Terms of Service</ListItem>
          </List>
        </Grid>
        <Grid item md={3}>
        <List>
            <ListItem>
              <ListItemText>Contact</ListItemText>
            </ListItem>
            <ListItem>Email</ListItem>
            <ListItem>Phone</ListItem>
            <ListItem>Telegram</ListItem>
          </List>
        </Grid>
        <Grid item md={3}>
        <List>
            <ListItem>
              <ListItemText>FOR MERCHANTS</ListItemText>
            </ListItem>
            <ListItem>Advertise on Varroe</ListItem>
            <ListItem>Merchant Support</ListItem>
            <ListItem>Blog For Merchants</ListItem>
          </List>
        </Grid>
      </Grid>
      <FooterCopyRightSection>
        <Grid container>
          <Grid item md={6}>
            Copyright 2021 Varroe. All RIghts Reserved
          </Grid>
          <Grid item md={6}>
            Terms and Conditions
          </Grid>
        </Grid>
      </FooterCopyRightSection>
    </StyledContainer>
  );
}

export default Footer;
