import React from "react";
// Functions & Hooks
import { useLocation, useNavigate } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../app/store";
import { logout, selectAuth } from "../../features/Authentication";
// Core Components
import { SearchBar } from "../Bar";
// Design & Icon Components
import {
  AppBar,
  Box,
  Button,
  IconButton,
  Toolbar,
  Container,
  Avatar,
} from "@mui/material";
import MenuIcon from "@mui/icons-material/Menu";
import { LogoIcon } from "../../assets/svg";
import { FillButton } from "../../ui/Buttons";

function NavBar() {
  const location = useLocation();
  const { pathname } = location;
  const isHome = pathname === "/";
  const isAuth = pathname === "/auth" || pathname === "/reset-password";
  const isProfile = pathname === "/profile";
  const dispatch = useAppDispatch();
  const { isAuthenticated, user } = useAppSelector(selectAuth);

  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] =
    React.useState<null | HTMLElement>(null);

  const navigator = useNavigate();
  if (isAuth) {
    return null;
  }
  const isMenuOpen = Boolean(anchorEl);
  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

  const handleProfileMenuOpen = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
    handleMobileMenuClose();
  };

  const handleMobileMenuOpen = (event: React.MouseEvent<HTMLElement>) => {
    setMobileMoreAnchorEl(event.currentTarget);
  };

  return (
    <AppBar
      position={isHome ? "absolute" : "sticky"}
      sx={{
        backgroundColor: isHome ? "transparent" : "#251d2b",
        marginBottom: isHome ? "-130px" : "0px",
        boxShadow: isHome ? "none" : "0px 2px 4px rgba(0, 0, 0, 0.25)",
      }}
    >
      <Container
        sx={{
          padding: "5px",
        }}
      >
        <Toolbar>
          <Box
            sx={{
              display: {
                sm: "flex",
                md: "none",
              },
            }}
          >
            <IconButton
              size="large"
              edge="start"
              color="inherit"
              aria-label="open drawer"
              sx={{ mr: 2 }}
            >
              <MenuIcon />
            </IconButton>
          </Box>
          <IconButton onClick={() => navigator("/")}>
            <LogoIcon width={160} height={76} />
          </IconButton>
          {isHome || isProfile ? null : (
            <Box
              sx={{
                display: {
                  xs: "none",
                  sm: "none",
                  md: "flex",
                },
              }}
            >
              <SearchBar />
            </Box>
          )}
          <Box sx={{ flexGrow: 1 }} />
          <Box sx={{ display: { xs: "none", md: "flex" } }}>
            <Button
              variant="text"
              sx={{
                color: "white",
                textTransform: "none",
                fontFamily: "Montserrat",
                ':hover': {
                  border: "2px solid white",
                },
              }}
              onClick={() => navigator("/marketplace")}
            >
              MarketPlace
            </Button>
            {!isAuthenticated ? (
              <>
                <FillButton color="inherit">LOGIN</FillButton>
                <Button sx={{
                  color: "white",
                  border: "1px solid white",
                  fontWeight: "bold",
                  ':hover': {
                    border: "2px solid #5c0e90",
                  },
                }} onClick={() => navigator("/auth")} variant="outlined">
                  SIGN UP
                </Button>
                <IconButton>
                  <svg
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M20.0831 10.5L21.2851 11.221C21.3593 11.2654 21.4206 11.3283 21.4633 11.4034C21.5059 11.4786 21.5283 11.5636 21.5283 11.65C21.5283 11.7364 21.5059 11.8214 21.4633 11.8966C21.4206 11.9718 21.3593 12.0346 21.2851 12.079L12.0001 17.65L2.7151 12.079C2.64095 12.0346 2.57956 11.9718 2.53695 11.8966C2.49433 11.8214 2.47192 11.7364 2.47192 11.65C2.47192 11.5636 2.49433 11.4786 2.53695 11.4034C2.57956 11.3283 2.64095 11.2654 2.7151 11.221L3.9171 10.5L12.0001 15.35L20.0831 10.5ZM20.0831 15.2L21.2851 15.921C21.3593 15.9654 21.4206 16.0283 21.4633 16.1034C21.5059 16.1786 21.5283 16.2636 21.5283 16.35C21.5283 16.4364 21.5059 16.5214 21.4633 16.5966C21.4206 16.6718 21.3593 16.7346 21.2851 16.779L12.5151 22.041C12.3596 22.1345 12.1815 22.1838 12.0001 22.1838C11.8187 22.1838 11.6406 22.1345 11.4851 22.041L2.7151 16.779C2.64095 16.7346 2.57956 16.6718 2.53695 16.5966C2.49433 16.5214 2.47192 16.4364 2.47192 16.35C2.47192 16.2636 2.49433 16.1786 2.53695 16.1034C2.57956 16.0283 2.64095 15.9654 2.7151 15.921L3.9171 15.2L12.0001 20.05L20.0831 15.2ZM12.5141 1.30901L21.2851 6.57101C21.3593 6.6154 21.4206 6.67826 21.4633 6.75344C21.5059 6.82863 21.5283 6.91358 21.5283 7.00001C21.5283 7.08643 21.5059 7.17139 21.4633 7.24657C21.4206 7.32176 21.3593 7.38462 21.2851 7.42901L12.0001 13L2.7151 7.42901C2.64095 7.38462 2.57956 7.32176 2.53695 7.24657C2.49433 7.17139 2.47192 7.08643 2.47192 7.00001C2.47192 6.91358 2.49433 6.82863 2.53695 6.75344C2.57956 6.67826 2.64095 6.6154 2.7151 6.57101L11.4851 1.30901C11.6406 1.21557 11.8187 1.1662 12.0001 1.1662C12.1815 1.1662 12.3596 1.21557 12.5151 1.30901H12.5141Z"
                      fill="white"
                    />
                  </svg>
                </IconButton>
              </>
            ) : (
              <>
                <FillButton onClick={() => dispatch(logout())}>
                  LOGOUT
                </FillButton>

                <Avatar onClick={() => navigator("/profile")}>
                  {user.email.split("@")[0]}
                </Avatar>
              </>
            )}
          </Box>
        </Toolbar>
      </Container>
    </AppBar>
  );
}

export default NavBar;
