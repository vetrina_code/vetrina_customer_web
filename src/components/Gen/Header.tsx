import React from "react";
// Functions & Hooks
import { useAppDispatch, useAppSelector } from "../../app/store";
import { selectAuth, logout } from "../../features/Authentication";

// Core Components
import { SearchBar } from "../Bar";
// Design & Icon Components
import LogoIcon from "../../assets/svg/LogoIcon";
import styled, { css } from "styled-components";
import {
  Grid,
  Container,
  IconButton,
  Avatar,
  Button,
  Stack,
} from "@mui/material";

import {
  TransparentButtton,
  FillButton,
  OutlinedButton,
} from "../../ui/Buttons";
import { useLocation, useNavigate } from "react-router-dom";

const StyledContainer = styled(Container)<{ color: string }>(
  ({ color }) => css`
    // height: 103px;
    background: ${color === "transparent" ? color : "#251d2b"};
    margin-bottom: ${color === "transparent" ? "-130px" : ""};
    padding-left: 50px;
    z-index: 10;
    font-family: "Montserrat";
    font-weight: 700;
    font-size: 11px;
    font-line-height: 13.41px;
    max-width: ${color === "transparent" ? "1200px" : "100% !important"};
  `
);

const GridItem = styled(Grid)`
  margin-top: 20px !important;
  margin-right: 15px !important;
`;

interface Props {
  color: string;
}

function Header({ color }: Props) {
  const location = useLocation();
  const { pathname } = location;
  const navigator = useNavigate();
  const dispatch = useAppDispatch();
  const { isAuthenticated, user } = useAppSelector(selectAuth);
  return (
    <>
      <StyledContainer color={color}>
        <Grid container>
          <Grid item md>
            <Stack direction="row">
              <IconButton onClick={() => navigator("/")}>
                <LogoIcon width={150} height={70} />
              </IconButton>
              <div
                style={{
                  paddingTop: 10,
                  width: "100%",
                }}
              >
                {!color && pathname !== "/profile" && <SearchBar />}
              </div>
            </Stack>
          </Grid>

          <GridItem item md={1.5}>
            <Button
              variant="text"
              sx={{
                color: "white",
                textTransform: "none",
              }}
              onClick={() => navigator("/marketplace")}
            >
              MarketPlace
            </Button>
          </GridItem>
          {!isAuthenticated ? (
            <>
              <GridItem item md={1.5}>
                <Button variant="contained" onClick={() => navigator("/auth")}>
                  LOGIN
                </Button>
              </GridItem>
              <GridItem item md={1.5}>
                <Button onClick={() => navigator("/auth")} variant="outlined">
                  SIGN UP
                </Button>
              </GridItem>
              <GridItem item md={1.5}>
                <IconButton>
                  <svg
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M20.0831 10.5L21.2851 11.221C21.3593 11.2654 21.4206 11.3283 21.4633 11.4034C21.5059 11.4786 21.5283 11.5636 21.5283 11.65C21.5283 11.7364 21.5059 11.8214 21.4633 11.8966C21.4206 11.9718 21.3593 12.0346 21.2851 12.079L12.0001 17.65L2.7151 12.079C2.64095 12.0346 2.57956 11.9718 2.53695 11.8966C2.49433 11.8214 2.47192 11.7364 2.47192 11.65C2.47192 11.5636 2.49433 11.4786 2.53695 11.4034C2.57956 11.3283 2.64095 11.2654 2.7151 11.221L3.9171 10.5L12.0001 15.35L20.0831 10.5ZM20.0831 15.2L21.2851 15.921C21.3593 15.9654 21.4206 16.0283 21.4633 16.1034C21.5059 16.1786 21.5283 16.2636 21.5283 16.35C21.5283 16.4364 21.5059 16.5214 21.4633 16.5966C21.4206 16.6718 21.3593 16.7346 21.2851 16.779L12.5151 22.041C12.3596 22.1345 12.1815 22.1838 12.0001 22.1838C11.8187 22.1838 11.6406 22.1345 11.4851 22.041L2.7151 16.779C2.64095 16.7346 2.57956 16.6718 2.53695 16.5966C2.49433 16.5214 2.47192 16.4364 2.47192 16.35C2.47192 16.2636 2.49433 16.1786 2.53695 16.1034C2.57956 16.0283 2.64095 15.9654 2.7151 15.921L3.9171 15.2L12.0001 20.05L20.0831 15.2ZM12.5141 1.30901L21.2851 6.57101C21.3593 6.6154 21.4206 6.67826 21.4633 6.75344C21.5059 6.82863 21.5283 6.91358 21.5283 7.00001C21.5283 7.08643 21.5059 7.17139 21.4633 7.24657C21.4206 7.32176 21.3593 7.38462 21.2851 7.42901L12.0001 13L2.7151 7.42901C2.64095 7.38462 2.57956 7.32176 2.53695 7.24657C2.49433 7.17139 2.47192 7.08643 2.47192 7.00001C2.47192 6.91358 2.49433 6.82863 2.53695 6.75344C2.57956 6.67826 2.64095 6.6154 2.7151 6.57101L11.4851 1.30901C11.6406 1.21557 11.8187 1.1662 12.0001 1.1662C12.1815 1.1662 12.3596 1.21557 12.5151 1.30901H12.5141Z"
                      fill="white"
                    />
                  </svg>
                </IconButton>
              </GridItem>
            </>
          ) : (
            <>
              <GridItem item md onClick={() => dispatch(logout())}>
                <FillButton>LOGOUT</FillButton>
              </GridItem>
              <GridItem item md>
                <Avatar onClick={() => navigator("/profile")}>
                  {user.email.split("@")[0]}
                </Avatar>
              </GridItem>
            </>
          )}
        </Grid>
      </StyledContainer>
    </>
  );
}

export default Header;
