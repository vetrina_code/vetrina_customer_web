export interface AuthenticationAttributesI {
  first_name?: string;
  last_name?: string;
  email: string;
  phone?: string;
  password: string;
  photo?: string;
  account_type?: string;
}