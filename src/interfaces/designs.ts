import { ButtonProps } from "@mui/material";
import { AuthenticationAttributesI } from "./forms";

export interface SvgProps {
  height: number | string;
  width: number | string;
}

export type Modify<T, R> = Omit<T, keyof R> & R;

export interface ExtendedButtonProps extends ButtonProps {
  mycolor?: 'black' | "purple" | 'whitesmoke' | 'red' | 'darkgrey';
  textcolor?: 'white' | 'black';
  preferredfont?: string
}

export type modalSize = 'sm' | 'md' | 'lg' | 'xl';

// Component Props Iterfaces
export interface AuthenticationComponentProps {
  handleRedirect: (step: AuthSteps) => void | undefined,
  handleUserDetails: (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => void,
  handleAuthenticationRequest: () => void,
  userDetails: AuthenticationAttributesI,
}
export interface ModalProps {
  open: boolean;
  selectedValue: string;
  onClose: () => void;
}

export type PeopleDetails = {
  name: string;
  price: number;
  image: string;
  isAvailable: boolean;
  availableTime: TimeSlots;
  availableDate: DateSlots;
}

export type ServiceDetails = {
  name: string;
  description: string;
  price: number;
  duration: number;
  currency: string;
  options: {
    label: string;
    values: {
      label: string;
      price: number;
    }[];
  }[];
}
export type TimeSlot = {
  start?: string;
  end?: string;
}
export type DateSlot = string;
export type TimeSlots = TimeSlot[]
export type DateSlots = DateSlot[]
export interface BusinessDetails {
  name: string;
  address: string;
  phone: string;
  website: string;
  service: ServiceDetails;
  availableDates: {
    date: DateSlot,
    availablePeople: PeopleDetails[];
  }[]
}

export type ServiceSteps = "customize" | "date" | "time" | "extras" | "confirmed";

export type AuthSteps = 'login' | 'register';
export type SelectedServiceOptions = {
  service?: ServiceDetails;
  person?: PeopleDetails;
  date?: string;
  time?: string;
  addOns?: Array<{}>;
  notes?: string,
}
export interface ServiceViewProps {
  handleStepChange: (step: ServiceSteps) => void;
  service: BusinessDetails;
  selectedOption: SelectedServiceOptions;
  handleSetSelectedOption?: any
  handleClose?(): void
}