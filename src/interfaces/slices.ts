// Contains all the types, enum, interfaces for redux toolkit slices


// Types, Enum & Interfaces for Authentication Slice
export type UserT = {
  first_name: string;
  last_name: string;
  email: string;
  id?: string;
  account_type?: string;
}

export interface AuthPayloadAttributesI {
  isAdmin?: boolean;
  token: string;
  user: UserT;
  isAuthenticated?: boolean;
}

export interface MarketPlaceStateI {
  page?: number,
  location: string;
  catgeory?: MarketPlaceCategoriesI;
  businesses: MarketPlaceBusinessI[];
}
export interface MarketPlaceFilterQueryI {
  key: string;
  value: string;
};
export interface MarketPlaceCategoriesI {
  _id: string;
  name: string;
  created_at: string;
  updated_at: string;
  icon_url: string;
}
export interface MarketPlaceBusinessI {
  "_id": string;
  "user_id": string;
  "registration_number": number;
  "business_name": string;
  "start_date": Date;
  "logo": string;
  "website_url": string;
  "street": string;
  "province": string;
  "city": string
  "country": string;
  "business_type": string;
  "staff_size": string;
  "primary_language": string;
  "government_id": string;
  "utility_bill": string;
  "business_email": string;
  "is_other_service_registered": boolean;
  "service_name": string;
  "service_url": string;
  "category": {
    _id: string;
    name: string;
    icon_url: string;
    created_at: string;
    updated_at: string;
  };
  "average_rating": number;
  "average_price": number;
  "business_description": string;
  "is_registered": {
    "country": string;
    "answer": boolean;
  },
  "created_at": Date;
  "updated_at": Date;
}
export type MarketPlaceBusinessesI = MarketPlaceBusinessI[];

export interface MerchantProfileI {
  addOns: BusinessServiceAddOnAttributesI[];
  business: string;
  created_at: Date;
  customizations: {
    name: string;
    _id: string;
    values: string[]
  }[];
  description: string;
  duration: string;
  images: string[];
  name: string;
  price: number;
  staff_accessibility: boolean;
  updated_at: Date;
  __v: number;
  _id: string;
}


export interface BusinessServiceAddOnAttributesI {
  name: string;
  price: number;
  _id: string;
}
export interface BusinessServiceCustomizationAttributes {
  name: string;
  values: string[];
  _id: string | number
}

export interface BusinessServicesI {

  _id: string;
  "name": string;
  "price": number;
  "duration": string;
  "description": string;
  "images": string[];
  "addOns": BusinessServiceAddOnAttributesI[];
  "customizations": BusinessServiceCustomizationAttributes[];
  "staff_accessibility": boolean,
  "business": string,
  "created_at": Date,
  updated_at: Date,
  __v: number
}