import { Typography } from "@mui/material";
import styled from "styled-components";

const Subtitle = styled(Typography)`
  font-family: "Montserrat";
  font-weight: 700;
  font-size: 84px;
  padding-bottom: 30px;
  text-transform: upppercase;
`;

export default Subtitle;