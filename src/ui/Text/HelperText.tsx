import { styled } from "@mui/material";

export const HelperText = styled('span')`
  font-size: 10px;
  font-family: Open Sans;
  font-weight: 400;
  margin: 5px 5px;
`