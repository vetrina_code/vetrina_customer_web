import { Typography } from "@mui/material";
import styled from "styled-components";

const Title = styled(Typography)`
  font-family: "Montserrat";
  font-weight: "700";
  font-style: "normal";
  font-weight: 400;
  font-size: 2.125rem;
  line-height: 1.235;
  letter-spacing: 0.00735em
`;

export default Title;