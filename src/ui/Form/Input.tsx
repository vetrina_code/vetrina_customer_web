import styled from "styled-components";

export const Input = styled.input`
  font-family: "Open Sans";
  font-size: 14px;
  border: none;
  "&:before": {
    border: none;
  }
  "&:after": {
    border: none;
  }
  ":focus": {
    outline: none;
  }
  width: 100%;
`
