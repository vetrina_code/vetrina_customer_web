import { FilledInput, styled, FilledInputProps} from "@mui/material";

export const FillInput = styled(FilledInput)<FilledInputProps>(() => ({
  fontFamily: "Ubuntu Mono",
  fontSize: "14px",
  border: "none",
  "&:before": {
    border: "none",
  },
  "&:after": {
    border: "none",
  },
  ":focus": {
    outline: "none",
  },
  width: "100%",
}))