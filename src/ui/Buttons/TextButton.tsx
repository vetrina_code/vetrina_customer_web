import styled, { StyledComponent } from "@emotion/styled";
import { Button } from "@mui/material";
import { ExtendedButtonProps } from "../../interfaces/designs";



const TextButton: StyledComponent<ExtendedButtonProps> = styled(Button)<ExtendedButtonProps>(({mycolor: myColor, preferredfont: preferredFont}) => ({
  // height: "35px",
  borderRadius: "5px",
  color: myColor ? myColor : "#5c0e90",
  width: "max-content",
  border: 0,
  fontWeight: 700,
  cursor: "pointer",
  fontFamily: preferredFont ? preferredFont : 'inherit',
  textTransform: 'capitalize',
  padding: 0
}))

export default TextButton;