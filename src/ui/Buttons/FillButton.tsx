import styled, { StyledComponent } from "@emotion/styled";
import { Button } from "@mui/material";
import { ExtendedButtonProps } from "../../interfaces/designs";



const FillButton: StyledComponent<ExtendedButtonProps> = styled(Button)<ExtendedButtonProps>(({mycolor: myColor, preferredfont: preferredFont, textcolor: textColor}) => ({
  height: "35px",
  borderRadius: "5px",
  color: myColor === 'black' ? "white" : (textColor || ''),
  background:  myColor ? myColor : "#5c0e90",
  width: "max-content",
  border: 0,
  fontWeight: 700,
  margin: "0 20px",
  cursor: "pointer",
  padding: "20px",
  fontFamily: preferredFont ? preferredFont : 'inherit',
  textTransform: 'capitalize'
}))

export default FillButton;