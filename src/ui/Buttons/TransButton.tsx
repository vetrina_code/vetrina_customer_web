import styled from "styled-components";

const TransparentButtton = styled.button`
  height: 35px;
  border-radius: 6px;
  color: white;
  background: #5c0e90;
  width: 100px;
  border: 0;
  font-weight: 700;
  background: transparent;
  margin: 0 10px;
  cursor: pointer;
`;

export default TransparentButtton;