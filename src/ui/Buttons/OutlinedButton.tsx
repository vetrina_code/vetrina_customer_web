import styled from "styled-components";

const SignUpButton = styled.button`
  height: 35px;
  border-radius: 6px;
  border: 2px solid white;
  color: white;
  background: #5c0e90;
  width: 100px;
  font-weight: 700;
  background: transparent;
  margin: 0 20px;
  cursor: pointer;
`;

export default SignUpButton;