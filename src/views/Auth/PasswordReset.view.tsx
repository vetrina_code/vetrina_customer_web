import React from "react";
import { useNavigate } from "react-router-dom";
import { Box, Card, CardContent, FormControl } from "@mui/material";
import { LogoIcon } from "../../assets/svg";
import { BackgroundWrapper, CenteredStack } from "../../components/Auth";
import { FillInput } from "../../ui/Form/FilledInput";
import { HelperText } from "../../ui/Text/HelperText";
import { FillButton, TextButton } from "../../ui/Buttons";

export default function PasswordResetView() {
  const [email, setEmail] = React.useState("");
  const naviagtor = useNavigate();

  const handleSetEmail = (email: string) => {
    setEmail(email);
  };
  return (
    <BackgroundWrapper
      container
      direction="column"
      justifyContent="space-evenly"
      alignItems="center"
    >
      <Box>
        <LogoIcon height={200} width={200} />
      </Box>

      <Card
        sx={{
          maxWidth: "400px",
          width: "100%",
          justifyContent: "center",
          alignItems: "center",
          textAlign: "center",
          fontFamily: "Open Sans !important",
        }}
      >
        <CardContent>
          <CenteredStack spacing={4}>
            <FormControl
              fullWidth
              size="small"
              sx={{
                alignItems: "flex-start",
              }}
            >
              <HelperText>Email</HelperText>
              <FillInput
                name="email"
                onChange={(event) => handleSetEmail(event.target.value)}
                margin="dense"
                placeholder="johndoe@example.com"
                value={email}
                id="email-input"
                aria-describedby="email"
              />
            </FormControl>
            <FillButton
            sx={{ color: "white", textTransform: "uppercase !important" }}
            onClick={() => {}}
          >
            RESET PASSWORD
          </FillButton>

          <span style={{fontSize: "12px"}}>Remebered ? <TextButton onClick={() => naviagtor('/auth')} variant='text'>Login</TextButton></span>
          </CenteredStack>
        </CardContent>
      </Card>
    </BackgroundWrapper>
  );
}