import React from "react";
import { FetchBaseQueryError } from "@reduxjs/toolkit/dist/query";
// Functions
import { setUser, selectAuth } from "../../features/Authentication";
import { setAppState } from "../../features/App";
// Hooks
import {
  useLoginMutation,
  useRegisterMutation,
} from "../../service/auth.service";
import { useAppDispatch, useAppSelector} from "../../app/store";
// Core Components
import Login from "../../components/Auth/Login";
import Register from "../../components/Auth/Register";
// Types & Interfaces
import { AuthSteps } from "../../interfaces/designs";
import { AuthenticationAttributesI } from "../../interfaces/forms";
// Design & Icon Components
import {
  Box,
} from "@mui/material";
import { LogoIcon } from "../../assets/svg";
import { TextButton } from "../../ui/Buttons";
import { BackgroundWrapper } from "../../components/Auth";
import { useNavigate } from "react-router-dom";

function Auth() {
  const dispatch = useAppDispatch();
  const navigator = useNavigate()
  const auth = useAppSelector(selectAuth);
  React.useEffect(() => {
    if (auth.user && auth.token) {
      navigator('/');
    }
  }, [auth, navigator]);
  const [step, setStep] = React.useState<AuthSteps>("login");
  const [userDetails, setUserDetails] =
    React.useState<AuthenticationAttributesI>({
      email: "",
      password: "",
    });
  const [
    login,
    {
      isError: isLoginError,
      isSuccess: isLoginSuccess,
      data: loginData,
      error: loginError,
    },
  ] = useLoginMutation();
  const [
    register,
    {
      isError: isRegistrationError,
      isSuccess: isRegistrationSuccess,
      data: registrationData,
      error: registrationError,
    },
  ] = useRegisterMutation();
  const handleStepChange = (step: AuthSteps) => {
    if (step === "login")
      setUserDetails((prevState) => {
        const newState = {
          email: prevState.email,
          password: prevState.password,
        };

        return newState;
      });
    if (step === "register")
      setUserDetails((prevState) => ({
        ...prevState,
        account_type: "customer",
        photo:
          "https://moonvillageassociation.org/wp-content/uploads/2018/06/default-profile-picture1.jpg",
      }));
    setStep(step);
  };
  const handleUserDetails = (
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    setUserDetails((prevState) => ({
      ...prevState,
      [event.target.name]: event.target.value,
    }));
  };
  const handleAuthenticationRequest = async () => {
    if (step === "login") {
      dispatch(setAppState({ isLoading: true }));
      await login(userDetails);
    }
    if (step === "register") {
      dispatch(setAppState({ isLoading: true }));
      await register(userDetails);
    }
  };

  React.useEffect(() => {
    if (isLoginSuccess) {
      dispatch(setAppState({ isLoading: false, isSuccess: true, success: "Login Successful" }));
      dispatch(setUser(loginData.data));
    }
    if (isRegistrationSuccess) {
      dispatch(setAppState({ isLoading: false, isSuccess: true, success: "Registration Successful" }));
      dispatch(setUser(registrationData.data));
    }
  }, [dispatch, isLoginSuccess, isRegistrationSuccess, loginData, registrationData]);

  React.useEffect(() => {
    if (isLoginError && (loginError as FetchBaseQueryError)["data"]) {
      const errorData = (loginError as FetchBaseQueryError)["data"] as Record<
        string,
        unknown
      >;
      const message = errorData.message;
      dispatch(setAppState({
        error: message as string,
        isError: true,
        isLoading: false,
      }));
    }
    if (
      isRegistrationError &&
      (registrationError as FetchBaseQueryError)["data"]
    ) {
      const errorData = (registrationError as FetchBaseQueryError)[
        "data"
      ] as Record<string, unknown>;
      const message = errorData.message;
      dispatch(setAppState({
        error: message as string,
        isError: true,
        isLoading: false,
      }));
    }
  }, [loginError, registrationError, isLoginError, isRegistrationError, dispatch]);

  return (
    <BackgroundWrapper
      container
      direction={step === "login" ? "column" : "row"}
      justifyContent="space-evenly"
      alignItems="center"
    >
      <Box sx={{
        cursor: "pointer",
      }}
        onClick={() => navigator("/")}
      >
        <LogoIcon height={200} width={200} />
      </Box>
      <Box>
        {step === "login" && (
          <Login
            handleRedirect={handleStepChange}
            handleUserDetails={handleUserDetails}
            handleAuthenticationRequest={handleAuthenticationRequest}
            userDetails={userDetails}
          />
        )}
        {step === "register" && (
          <Register
            handleRedirect={handleStepChange}
            handleUserDetails={handleUserDetails}
            handleAuthenticationRequest={handleAuthenticationRequest}
            userDetails={userDetails}
          />
        )}
        <Box
          sx={{
            color: "white",
            fontSize: "12px",
            fontFamily: "Open Sans",
            textAlign: "center",
          }}
        >
          <p>Get Your Business more visibility</p>
          <span>
            Create a <TextButton variant="text">merchant</TextButton> or{" "}
            <TextButton variant="text">freelancer</TextButton>
          </span>
        </Box>
      </Box>
    </BackgroundWrapper>
  );
}

export default Auth;
