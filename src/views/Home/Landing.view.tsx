import React from "react";
import { useNavigate } from "react-router-dom";
import styled from "styled-components";
// Core Components --------------------------------------------------------------
import SearchBar from "../../components/Bar/Search";
import MarketPlaceCategories from "../../components/Marketplace/Categories";
// Design & Icon Component
import {
  Stack,
  Grid,
  Box,
  Typography,
  Container,
  IconButton,
  Button,
} from "@mui/material";
import { HeroMoreInfo, LargeVarroeLogo } from "../../assets/svg";

const BackgroundWrapper = styled.div`
  font-family: "Montserrat";
  width: 100%;
  height: 100%;
  background-image: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)),
    url(/img/heroImage.png);
  background-size: cover;
  background-repeat: no-repeat;
  padding: 150px 0px;
`;

const TextButton = styled.div`
  font-family: "Montserrat";
  cursor: pointer;
`;

function Landing(): JSX.Element {
  const navigator = useNavigate();
  return (
    <Stack spacing={4}>
      <BackgroundWrapper>
        <Container sx={{ justifyContent: "space-evenly", color: "white" }}>
          <Grid container justifyContent={"center"}>
            <Grid item>
              <Box sx={{ width: "100%", maxWidth: 800 }}>
                <Typography
                  variant="h4"
                  sx={{
                    fontFamily: "Montserrat",
                    fontWeight: 700,
                    fontSize: "40px",
                    lineHeight: "78.02px",
                  }}
                >
                  DISCOVER THE BEST
                </Typography>
                <Typography
                  variant="h2"
                  sx={{
                    fontFamily: "Montserrat",
                    fontWeight: 700,
                    fontSize: "64px",
                    lineHeight: "78.02px",
                  }}
                >
                  LOCAL MERCHANTS
                </Typography>
                <Typography variant="caption" display="block" gutterBottom>
                  Varroe provides access to the best merchants
                </Typography>
              </Box>
            </Grid>
            <Grid item paddingLeft={"20px"}>
              <Grid container justifyContent={"center"}>
                <Grid item>
                  <HeroMoreInfo width={200} height={100} />
                </Grid>
                <Grid item paddingTop={"35px"}>
                  <TextButton onClick={(e) => console.log("REDIRECT ACTION")}>
                    <Typography variant="caption" display="block" gutterBottom>
                      {/* {storeName} */}
                      {"The Common Room"}
                    </Typography>
                    <Typography variant="caption" display="block" gutterBottom>
                      {/* {storeLocation} */}
                      {"TORONTO, ON"}
                    </Typography>
                  </TextButton>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
          <Grid container paddingTop={"100px"} justifyContent="center">
            <IconButton>
              <SearchBar />
            </IconButton>
          </Grid>
        </Container>
      </BackgroundWrapper>
      <Grid
        container
        sx={{
          padding: "20px 100px",
        }}
      >
        <Grid
          item
          lg={12}
          md={12}
          xs={12}
          sx={{
            textAlign: "center",
          }}
        >
          <Typography
            component={"h1"}
            sx={{
              fontFamily: "Montserrat",
              fontWeight: 700,
              fontSize: "48px",
              lineHeight: "58.13px",
              color: "rgba(92, 14, 144, 1)",
            }}
          >
            POPULAR CATEGORY
          </Typography>
        </Grid>
        <Grid item>
          <MarketPlaceCategories size="large" />
        </Grid>
      </Grid>
      <Grid
        container
        sx={{
          padding: "50px 150px",
          background: "rgba(37, 29, 42, 1)",
          justifyContent: "space-between",
          color: "white",
          fontFamily: "Montserrat !important",
        }}
      >
        <Stack>
          <Typography
            component={"h1"}
            fontSize={"38px"}
            fontFamily={"inherit"}
            fontWeight="bold"
          >
            OWN A
          </Typography>
          <h2
            style={{
              color: "rgba(92, 14, 144, 1)",
              fontFamily: "Montserrat",
              fontWeight: 700,
              fontSize: "38px",
            }}
          >
            BUSINESS ?
          </h2>
          <h5 style={{
            fontFamily: 'Open Sans',
            fontStyle: 'normal',
            fontWeight: 400,
            fontSize: '24px',
            lineHeight: '33px',
            margin: 0
          }}>Expand your reach! Get Started with us Today.</h5>
          <h5>SIGN UP AS A</h5>
          <Stack direction={"row"} spacing={5}>
            <Button
              sx={{
                fontFamily: "Montserrat",
                fontWeight: 700,
                fontSize: "15px",
                lineHeight: "38.13px",
                color: "rgba(92, 14, 144, 1)",
                background: "white",
              }}
              onClick={(e) => navigator('/auth')}
            >
              MERCHANT
            </Button>
            <Button
              sx={{
                fontFamily: "Montserrat",
                fontWeight: 700,
                fontSize: "15px",
                lineHeight: "38.13px",
                color: "rgba(92, 14, 144, 1)",
                background: "white",
              }}
            >
              FREELANCER
            </Button>
          </Stack>
        </Stack>

        <LargeVarroeLogo />
      </Grid>
    </Stack>
  );
}

export default Landing;
