import React from "react";
import { useNavigate } from "react-router-dom";
// Functions & Hooks -----------------------------------------------------------
import {
  useGetBusinessesQuery,
  useGetBuisnessByCategoryMutation,
} from "../../service/varroe/services/api";
import { useAppDispatch, useAppSelector } from "../../app/store";
import { setAppState } from "../../features/App";
import {
  selectBusinesses,
  selectLocation,
  selectPageNumber,
  setBusinesses,
  setPageNumber,
} from "../../features/Market";
// Core Components --------------------------------------------------------------
import MarketPlaceCategories from "../../components/Marketplace/Categories";
import { FilterModal } from "../../components/Modals";
import BusinessCard from "../../components/Marketplace/BusinessCard";
// Design Components -----------------------------------------------------------
import {
  Breadcrumbs,
  Grid,
  Link,
  Stack,
  Box,
  styled,
  Pagination,
} from "@mui/material";
import { TextButton } from "../../ui/Buttons";
import FilterIcon from "../../assets/svg/FilterIcon";
// Interfaces & Types -------------------------------------------------------------
import {
  MarketPlaceBusinessI,
  MarketPlaceFilterQueryI,
} from "../../interfaces/slices";
import { setActiveBusiness } from "../../features/Merchant";

const BackgroundWrapper = styled("div")`
  background: #e5e5e5;
  max-width: 100vw;
  font-family: "Montserrat";
  padding: 20px 0px;
`;

function MarketPlace() {
  const dispatch = useAppDispatch();
  const navigator = useNavigate();
  const location = useAppSelector(selectLocation);
  const category_name = new URLSearchParams(window.location.search).get(
    "category"
  );

  const [
    getBusinessesByCategory,
    { data: businessesByCategory, isLoading: categoryisLoading },
  ] = useGetBuisnessByCategoryMutation();

  React.useEffect(() => {
    if (category_name) {
      getBusinessesByCategory(category_name);
    }
  }, [category_name, getBusinessesByCategory]);

  React.useEffect(() => {
    console.log(businessesByCategory);
    if (businessesByCategory?.data) {
      dispatch(setBusinesses([...businessesByCategory.data]));
    }
  }, [businessesByCategory, dispatch]);

  const [showFilterModal, setModalDisplay] = React.useState(false);
  const handleShowFilterModal = () => {
    setModalDisplay(!showFilterModal);
  };
  const page = useAppSelector(selectPageNumber) || 1;
  const handlePageChange = (page: number) => {
    dispatch(setPageNumber);
  };

  const [filters, setFilters] = React.useState<MarketPlaceFilterQueryI[]>([
    {
      key: "sortby",
      value: "-createdAt",
    },
  ]);
  const {
    data: allBusinesses,
    isLoading,
    isFetching,
  } = useGetBusinessesQuery({
    page,
  });
  React.useEffect(() => {
    if (!category_name && allBusinesses && allBusinesses.data) {
      console.log("ALLBUSINESS", allBusinesses.data);
      dispatch(setBusinesses([...allBusinesses.data]));
    }
  }, [allBusinesses, category_name]);
  React.useEffect(() => {
    dispatch(setAppState({ isLoading: isLoading || isFetching }));
  }, [dispatch, isLoading, isFetching]);
  React.useEffect(() => {
    dispatch(setAppState({ isLoading: categoryisLoading }));
  }, [dispatch, categoryisLoading]);

  const businesses = useAppSelector(selectBusinesses);
  function handleBusinessSelect(business: MarketPlaceBusinessI): void {
    dispatch(setActiveBusiness(business));
    navigator("/merchant/" + business._id);
  }
  return (
    <>
      <FilterModal
        open={showFilterModal}
        onClose={handleShowFilterModal}
        selectedValue={""}
      />
      <Grid
        container
        sx={{
          padding: "20px 100px",
        }}
      >
        <Grid item md={12}>
          <Stack spacing={4}>
            <Breadcrumbs aria-label="breadcrumb">
              <Link underline="hover" color="inherit" href="/">
                {location}
              </Link>
              <Link underline="hover" color="inherit" href="/marketplace">
                {"All Merchants"}
              </Link>
            </Breadcrumbs>
            <MarketPlaceCategories size="small" />
            <Grid
              container
              spacing={4}
              direction="row"
              justifyContent="space-between"
              alignItems="flex-end"
            >
              <span
                style={{
                  fontWeight: "bold",
                  fontSize: "28px",
                  lineHeight: "34.13px",
                }}
              >
                Best Merchants in {location.toUpperCase()}
              </span>
              <TextButton
                onClick={() => handleShowFilterModal()}
                startIcon={<FilterIcon />}
              >
                Filter
              </TextButton>
            </Grid>
          </Stack>
        </Grid>
      </Grid>

      <BackgroundWrapper>
        <Grid
          container
          columns={12}
          columnSpacing={4}
          sx={{
            padding: "10px 50px",
          }}
        >
          {businesses.length &&
            businesses.map((business, index) => {
              // console.log(business);
              return (
                <Grid item md={4} sm={6} lg={4} xl={3} key={index}>
                  <Box
                    sx={{
                      padding: "1rem",
                      maxWidth: "350px",
                    }}
                    onClick={() => handleBusinessSelect(business)}
                  >
                    <BusinessCard business={business} />
                  </Box>
                </Grid>
              );
            })}
          {/* {allBusinesses ? allBusinesses?.data?.map((business: MarketPlaceBusinessI, index: React.Key | null | undefined) => (
            <Grid item md={4} sm={6} lg={4} xl={3} key={index}>
              <Box
                sx={{
                  padding: "1rem",
                  maxWidth: "350px",
                }}
                onClick={() => {
                  navigator("/merchant/" + business._id);
                }}
              >
                <BusinessCard business={business} />
              </Box>
            </Grid>
          )) : null} */}
        </Grid>
        <Grid container justifyContent={"flex-end"} alignItems={"flex-end"}>
          <Pagination
            count={10}
            variant="outlined"
            color="secondary"
            shape="rounded"
            page={page}
            onChange={(event, page) => handlePageChange(page)}
          />
        </Grid>
      </BackgroundWrapper>
    </>
  );
}

export default MarketPlace;
