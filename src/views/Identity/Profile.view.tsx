import React from 'react'
import { Avatar, Container, Stack, Grid, Box} from '@mui/material'
import { useAppSelector } from '../../app/store'
import { selectAuth } from '../../features/Authentication'
import { FillButton } from '../../ui/Buttons';
import { CustomTabs, CustomTab, TabPanel} from '../../components/Profile/Tabs';
import Appointment from '../../components/Profile/Appointment';
import Info from '../../components/Profile/Info';
import Review from '../../components/Profile/Review';
import Setting from '../../components/Profile/Setting';
import { useNavigate } from 'react-router-dom';

function ProfileView() {
  const { user, token} = useAppSelector(selectAuth);
  const navigator = useNavigate();
  React.useEffect(() => {
    if (!token) {
      navigator('/auth');
    }
  }, [token, navigator]);
  const [value, setValue] = React.useState(0);
  const handleSetValue = (event: React.SyntheticEvent, newValue: number) => setValue(newValue);
  return (
    <Container sx={{
      marginTop: '2rem',
    }}>
      <Stack spacing={3}>
        <Grid container justifyContent={'space-between'}>
          <Stack direction='row' spacing={3}>
            <Avatar src='https://picsum.photos/200' alt='avatar' sx={{
              width: '86px',
              height: '86px',
            }}/>
            <Stack direction='column' spacing={2} sx={{
              fontSize: '1.5rem',
              fontWeight: 500,
              fontFamily: 'Montserrat',
            }}>
              <span>Username</span>
              <span>{user.email}</span>
            </Stack>
          </Stack>
            <FillButton sx={{
              textTransform: 'uppercase !important',
              color: 'white',
            }}>
              logout
            </FillButton>

        </Grid>
        <Box sx={{
          width: '100%',
        }}>
          <CustomTabs value={value} onChange={handleSetValue} aria-label="basic tabs example">
            <CustomTab label='Appointments' value={0}/>
            <CustomTab label='Account Info' value={1}/>
            <CustomTab label='Reviews' value={2}/>
            <CustomTab label='Settings' value={3}/>
          </CustomTabs>
        </Box>

        <Box sx={{
          width: '100%',
          marginBottom: '7rem !important',
        }}>
          <TabPanel value={value} index={0}>
            <Appointment/>
          </TabPanel>
          <TabPanel value={value} index={1}>
            <Info />
          </TabPanel>
          <TabPanel value={value} index={2}>
            <Review />
          </TabPanel>
          <TabPanel value={value} index={3}>
            <Setting />
          </TabPanel>
        </Box>
      </Stack>
    </Container>
  )
}

export default ProfileView