import React from "react";
import { Grid, Stack, Box, Skeleton } from "@mui/material";
import { FillButton } from "../../ui/Buttons";
import ServiceCard from "../../components/Merchant/ServiceCard";
import { StarOutlined } from "@mui/icons-material";
import ReviewCard from "../../components/Merchant/ReviewCard";
import { ReviewModal, ServiceModal } from "../../components/Modals";
import { useParams } from "react-router-dom";
import { useGetBusinessServicesQuery } from "../../service/varroe/services/api";
import { useAppDispatch, useAppSelector } from "../../app/store";
import { setAppState } from "../../features/App";
import Cart from "../../components/Merchant/Cart";
import SideDrawer from "../../components/Merchant/SideDrawer";
import BusinessHeader from "../../components/Merchant/BusinessHeader";
import MerchantLocation from "../../components/Merchant/MerchantLocation";
import { selectBusiness, setActiveService } from "../../features/Merchant";
import { BusinessServicesI, MarketPlaceBusinessI } from "../../interfaces/slices";

function ProfileView() {
  const dispatch = useAppDispatch();
  const { id = "" } = useParams();
  const [reviewModal, setReviewModal] = React.useState(false);
  const [serviceModal, setServiceModal] = React.useState(false);

  const { isLoading, data: merchantProfileData } = useGetBusinessServicesQuery(id);
  React.useEffect(() => {
    dispatch(setAppState({ isLoading: isLoading }));
  }, [dispatch, isLoading]);
  const business = useAppSelector(selectBusiness) as MarketPlaceBusinessI;

  if (!merchantProfileData) {
    return <p>...loading</p>;
  }

  const services = merchantProfileData.data;

  function handleServiceSelect(service: BusinessServicesI) {
    dispatch(setActiveService(service));
    setServiceModal(true)
  }

  return (
    <React.Fragment>
      <ReviewModal
        open={reviewModal}
        handleClose={() => setReviewModal(false)}
      />

      <ServiceModal
        open={serviceModal}
        handleClose={() => setServiceModal(false)}
        businessKey={business.business_name === "Varroe Hair Styles" ? true : false}
      />

      <Grid
        columns={12}
        container
        direction="row"
        justifyContent="flex-start"
        alignItems="stretch"
      >
        <Grid item md={3}>
          <SideDrawer business={business} />
        </Grid>

        <Grid item md={9}>
          <Box sx={{
          }}>
            <BusinessHeader business={business} />
            {/* Service Section */}
            <Stack
              spacing={3}
              sx={{
                background: "rgba(246, 246, 246, 1);",
                padding: "40px",
              }}
            >
              <MerchantLocation business={business} />
              <h1>Service Offered</h1>

              <Grid container columns={10}>
                <Grid item md={7} container spacing={2} direction="row">
                  {services.map(
                  (service, index) => (
                    <Grid key={index} item md={6} onClick={() => handleServiceSelect(service)}>
                      <ServiceCard service={service} />
                    </Grid>
                  )
                )}
                </Grid>
                <Grid item md>
                  <Cart />
                </Grid>
              </Grid>
            </Stack>
            {/* Photos Section */}
            <Stack spacing={3} padding={"40px"}>
              <h4>Photos</h4>
              <Box sx={{ overflow: "scroll" }}>
                <Stack direction={"row"} spacing={3}>
                  {[0, 1, 2, 3, 4, 5].map((index) => {
                    return (
                      <img
                        key={index}
                        alt={`photos-${index}`}
                        src={
                          "https://images.unsplash.com/photo-1518791841217-8f162f1e1131?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80"
                        }
                        width={"220px"}
                        height={"162px"}
                      />
                    );
                  })}
                </Stack>
              </Box>
            </Stack>
            {/* Review Section */}
            <Stack
              spacing={3}
              padding={"40px"}
              sx={{
                background: "rgba(246, 246, 246, 1);",
              }}
            >
              <h4>
                Reviews
                <span>
                  <FillButton
                    startIcon={<StarOutlined />}
                    sx={{
                      color: "white",
                    }}
                    onClick={() => setReviewModal(true)}
                  >
                    Make A Review
                  </FillButton>
                </span>
              </h4>
              <Grid container spacing={3}>
                {[0, 1, 2, 3, 4, 5].map((index) => {
                  return (
                    <Grid key={index} item md>
                      <ReviewCard />
                    </Grid>
                  );
                })}
              </Grid>
            </Stack>
          </Box>
        </Grid>
      </Grid>
    </React.Fragment>
  );
}

export default ProfileView;
