// import { RequestOptions } from '@octokit/types/dist-types/RequestOptions';
import { BaseQueryFn } from '@reduxjs/toolkit/query/react';
import axios, { AxiosError } from 'axios';
import { RootState } from '../../app/store';

const VarroeAxiosInstance = axios.create({
  baseURL: 'http://ec2-34-245-84-35.eu-west-1.compute.amazonaws.com:3005/api/v1/',
  headers: {
    accept: 'application/json',
  }
})

const axiosBaseQuery = (): BaseQueryFn => async (requestOpts, { getState }) => {
  try {
    const token = (getState() as RootState).auth.token;
    const result = await VarroeAxiosInstance({
      ...requestOpts,
      headers: {
        Authorization: `Bearer ${token}`
      }
    });

    return {
      data: result.data,
    };
  } catch (axiosError) {
    const err = axiosError as AxiosError;
    return { error: { status: err.response?.status, data: err.response?.data } };
  }
};

export const VarroeBaseQuery = axiosBaseQuery();