import { createApi } from '@reduxjs/toolkit/query/react';
import { VarroeBaseQuery } from '..';
import { BusinessServicesI, MarketPlaceBusinessesI, MarketPlaceBusinessI, MarketPlaceCategoriesI, MarketPlaceFilterQueryI, MerchantProfileI } from '../../../interfaces/slices';


interface urlParams {
  page: number;
  filters?: MarketPlaceFilterQueryI[]
}

interface GetBusinessesResponse {
  status: string;
  data: MarketPlaceBusinessesI;
  message: string;
}
interface GetCategoriesResponse {
  status: string;
  data: MarketPlaceCategoriesI[];
  message: string;
}

interface GetBusinessServiceResponse {
  status: string;
  data: BusinessServicesI[];
  message: string;
}
export const marketServiceApi = createApi({
  reducerPath: 'marketPlace_service',
  baseQuery: VarroeBaseQuery,
  endpoints: function (builder) {
    return {
      // getBusinesses: builder.mutation({
      //   query: (page: number, filters?: MarketPlaceFilterQueryI[]) => {
      //     return {
      //       url: `/business?page=${page}&limit=45${filters?.map(filter => `&${filter.key}=${filter.value}`).join('')}`,
      //       method: 'get',
      //     };
      //   }
      // }),
      getBusinesses: builder.query<GetBusinessesResponse, urlParams>({
        query: (params: urlParams) => {
          const { page, filters } = params;
          return {
            url: filters
              ?
              `/business?page=${page}&limit=45${filters && filters.map(filter => `&${filter.key}=${filter.value}`).join('')}`
              :
              `/business?page=${page}&limit=45`,
            method: 'get',
          };
        }
      }),
      getCategories: builder.query<GetCategoriesResponse, void>({
        query: () => {
          return {
            url: '/category/get-categories',
            method: 'get',
          }
        }
      }),
      getBusinessServices: builder.query<GetBusinessServiceResponse, string>({
        query: (id: string) => {
          return {
            url: `/service/get-services-by-business/${id}`,
            method: 'get',
          }
        }
      }),
      getBuisnessByCategory: builder.mutation<GetBusinessesResponse, string>({
        query: (name: string) => {
          return {
            url: `/business/get-business-by-category/${name}`,
            method: 'get',
          }
        }
      })
    }
  }
});
export const {
  useGetBusinessesQuery,
  useGetCategoriesQuery,
  useGetBusinessServicesQuery,
  useGetBuisnessByCategoryMutation,
} = marketServiceApi;



export default marketServiceApi;