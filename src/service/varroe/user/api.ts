import { createApi } from '@reduxjs/toolkit/query/react';
import { VarroeBaseQuery } from '..';
import { UserT } from '../../../interfaces/slices';


export interface GetUserResponse {
  status: string;
  data: UserT,
  message: string;
}
export const userServiceApi = createApi({
  reducerPath: 'user_service',
  baseQuery: VarroeBaseQuery,
  endpoints: function (builder) {
    return {
      getProfile: builder.query<GetUserResponse, void>({
        query: () => {
          return {
            url: '/user/get-user-profile-by-token',
            method: 'get',
          };
        }
      }),
      updateProfile: builder.mutation<GetUserResponse, UserT>({
        query: (user: UserT) => {
          return {
            url: `/user/update-user-profile`,
            method: 'patch',
            body: user
          }
        }
      }),
      updatePassword: builder.mutation<GetUserResponse, string>({
        query: (password: string) => {
          return {
            url: `/user/update-user-password`,
            method: 'patch',
            body: password
          }
        }
      }),
    }
  }
});
export const {
  useGetProfileQuery,
  useUpdateProfileMutation,
  useUpdatePasswordMutation
} = userServiceApi;



export default userServiceApi;