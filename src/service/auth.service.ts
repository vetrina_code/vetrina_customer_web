import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import { AuthenticationAttributesI } from '../interfaces/forms';

export const auth_service = createApi({
  reducerPath: 'auth.service',
  baseQuery: fetchBaseQuery({
    baseUrl: 'http://ec2-34-245-84-35.eu-west-1.compute.amazonaws.com:3005/api/v1/',
  }),
  
  endpoints: function (builder) {
    return {
      register: builder.mutation({
        query: (userData: AuthenticationAttributesI) => {
          return {
            url: 'auth/customer/register',
            method: 'post',
            body: userData,
          };
        }
      }),
      login: builder.mutation({
        query: (userData: AuthenticationAttributesI) => ({
          url: 'auth/login',
          method: 'post',
          body: userData,
        })
      })
    };
  }
});
export const {
  useRegisterMutation,
  useLoginMutation,
} = auth_service;