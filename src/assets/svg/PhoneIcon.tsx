const PhoneIcon = () => {
  return (
    <svg
      width="53"
      height="53"
      viewBox="0 0 53 53"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g filter="url(#filter0_d_444_3501)">
        <path
          d="M4 22.5C4 10.0736 14.0736 0 26.5 0C38.9264 0 49 10.0736 49 22.5C49 34.9264 38.9264 45 26.5 45C14.0736 45 4 34.9264 4 22.5Z"
          fill="white"
        />
      </g>
      <path
        d="M37 26.92V29.92C37.0011 30.1985 36.9441 30.4742 36.8325 30.7294C36.7209 30.9845 36.5573 31.2136 36.3521 31.4019C36.1468 31.5901 35.9046 31.7335 35.6407 31.8227C35.3769 31.9119 35.0974 31.9451 34.82 31.92C31.7428 31.5856 28.787 30.5342 26.19 28.85C23.7738 27.3147 21.7253 25.2662 20.19 22.85C18.5 20.2412 17.4482 17.271 17.12 14.18C17.095 13.9035 17.1279 13.6248 17.2165 13.3616C17.3051 13.0985 17.4476 12.8567 17.6348 12.6516C17.822 12.4466 18.0498 12.2827 18.3038 12.1705C18.5578 12.0583 18.8323 12.0003 19.11 12H22.11C22.5953 11.9952 23.0658 12.1671 23.4338 12.4835C23.8017 12.8 24.0421 13.2395 24.11 13.72C24.2366 14.6801 24.4714 15.6227 24.81 16.53C24.9445 16.8879 24.9737 17.2769 24.8939 17.6509C24.8141 18.0249 24.6289 18.3681 24.36 18.64L23.09 19.91C24.5135 22.4136 26.5864 24.4865 29.09 25.91L30.36 24.64C30.6319 24.3711 30.9751 24.1859 31.3491 24.1061C31.7231 24.0263 32.1121 24.0555 32.47 24.19C33.3773 24.5286 34.3199 24.7634 35.28 24.89C35.7658 24.9585 36.2094 25.2032 36.5265 25.5775C36.8437 25.9518 37.0122 26.4296 37 26.92Z"
        stroke="#251D2A"
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <defs>
        <filter
          id="filter0_d_444_3501"
          x="0"
          y="0"
          width="53"
          height="53"
          filterUnits="userSpaceOnUse"
          colorInterpolationFilters="sRGB"
        >
          <feFlood floodOpacity="0" result="BackgroundImageFix" />
          <feColorMatrix
            in="SourceAlpha"
            type="matrix"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
            result="hardAlpha"
          />
          <feOffset dy="4" />
          <feGaussianBlur stdDeviation="2" />
          <feComposite in2="hardAlpha" operator="out" />
          <feColorMatrix
            type="matrix"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0"
          />
          <feBlend
            mode="normal"
            in2="BackgroundImageFix"
            result="effect1_dropShadow_444_3501"
          />
          <feBlend
            mode="normal"
            in="SourceGraphic"
            in2="effect1_dropShadow_444_3501"
            result="shape"
          />
        </filter>
      </defs>
    </svg>
  );
};

export default PhoneIcon;
