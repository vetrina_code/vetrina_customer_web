const WhiteCloseIcon = () => {
  return (
    <svg
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M16.192 5.34375L11.949 9.58575L7.70697 5.34375L6.29297 6.75775L10.535 10.9998L6.29297 15.2418L7.70697 16.6558L11.949 12.4137L16.192 16.6558L17.606 15.2418L13.364 10.9998L17.606 6.75775L16.192 5.34375Z"
        fill="white"
      />
    </svg>
  );
};

export default WhiteCloseIcon;
