import React from "react";

const FilterIcon = () => {
  return (
    <svg
      width='20px'
      height='20px'
      viewBox="0 0 16 14"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M15.5 12.8333H5.5M0.5 1.16667H3.83333H0.5ZM15.5 1.16667H7.16667H15.5ZM0.5 7H10.5H0.5ZM15.5 7H13.8333H15.5ZM0.5 12.8333H2.16667H0.5Z"
        stroke="black"
        strokeLinecap="round"
      />
    </svg>
  );
};

export default FilterIcon;
