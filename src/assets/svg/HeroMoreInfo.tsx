import React from "react";

export interface SvgProps {
  height: number;
  width: number;
}
const HeroMoreInfo = ({ height, width }: SvgProps) => {
  return (
    <svg
      width={width}
      height={height}
      viewBox="0 0 149 72"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <line
        x1="98.8346"
        y1="27.4718"
        x2="21.8346"
        y2="0.471835"
        stroke="white"
      />
      <path
        d="M140.53 21.7189L122.25 11.1651C119.311 9.46805 115.689 9.46805 112.75 11.1651L94.4702 21.7189C91.5309 23.4159 89.7202 26.5521 89.7202 29.9462V51.0538C89.7202 54.4479 91.5309 57.5841 94.4702 59.2811L112.75 69.8349C115.689 71.5319 119.311 71.5319 122.25 69.8349L140.53 59.2811C143.469 57.5841 145.28 54.4479 145.28 51.0538V29.9462C145.28 26.5521 143.469 23.4159 140.53 21.7189Z"
        fill="#251D2A"
        stroke="white"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M121.028 52.584H112.969L121.763 27.4453H129.821L121.028 52.584Z"
        fill="#5C0E90"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M111.682 47.7596H119.745L114.242 32.2696H106.179L111.682 47.7596Z"
        fill="white"
      />
      <line
        x1="22.4021"
        y1="0.297196"
        x2="5.40209"
        y2="23.2972"
        stroke="white"
      />
      <circle cx="3.5" cy="25.5" r="3" stroke="white" />
    </svg>
  );
};

export default HeroMoreInfo;
