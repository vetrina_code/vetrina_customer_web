const BlackCloseIcon = () => {
  return (
    <svg
      width="124"
      height="124"
      viewBox="0 0 124 124"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M113.665 57.2466V61.9999C113.659 73.1415 110.051 83.9824 103.38 92.906C96.7092 101.83 87.3323 108.358 76.648 111.517C65.9638 114.676 54.5445 114.296 44.0935 110.435C33.6424 106.574 24.7194 99.4381 18.6553 90.0915C12.5912 80.7448 9.71095 69.6883 10.444 58.5709C11.1771 47.4535 15.4843 36.871 22.7231 28.4015C29.962 19.932 39.7448 14.0294 50.6123 11.574C61.4799 9.11865 72.8501 10.242 83.0271 14.7766"
        stroke="#34A853"
        stroke-width="5"
        strokeLinecap="round"
        stroke-linejoin="round"
      />
      <path
        d="M113.667 20.666L62 72.3844L46.5 56.8844"
        stroke="#34A853"
        stroke-width="5"
        strokeLinecap="round"
        stroke-linejoin="round"
      />
    </svg>
  );
};

export default BlackCloseIcon;
