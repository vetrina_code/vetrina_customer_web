const GoFrontIcon = () => {
  return (
    <svg
      width="10"
      height="10"
      transform="translate(4 4) rotate(180)"
      viewBox="0 0 10 10"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g clipPath="url(#clip0_300_9612)">
        <path
          d="M3.58268 2L0.666016 4.91667L3.58268 7.83333"
          stroke="#111111"
          strokeWidth="0.7"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path
          d="M0.666016 4.9165H7.33268"
          stroke="#111111"
          strokeWidth="0.7"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
      </g>
      {/* <defs>
        <clipPath id="clip0_300_9612">
        <rect
          width="4"
          height="4"
          fill="black"
          transform="translate(4 4) rotate(180)"
        />
        </clipPath>
      </defs> */}
    </svg>
  );
};

export default GoFrontIcon;
