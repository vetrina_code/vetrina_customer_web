const MailIcon = () => {
  return (
    <svg width="53" height="53" viewBox="0 0 53 53" fill="none" xmlns="http://www.w3.org/2000/svg">
<g filter="url(#filter0_d_444_3500)">
<path d="M4 22.5C4 10.0736 14.0736 0 26.5 0C38.9264 0 49 10.0736 49 22.5C49 34.9264 38.9264 45 26.5 45C14.0736 45 4 34.9264 4 22.5Z" fill="white"/>
</g>
<g clipPath="url(#clip0_444_3500)">
<path d="M26.5853 15.7786C28.4176 15.7786 30.1441 16.5908 31.4093 17.8606V17.8643C31.4093 17.2546 31.8196 16.7933 32.3866 16.7933H32.5306C33.4253 16.7933 33.6046 17.6371 33.6046 17.9033L33.6083 27.3796C33.5461 28.0006 34.2496 28.3216 34.6403 27.9226C36.1591 26.3603 37.9793 19.8833 33.6946 16.1333C29.6986 12.6331 24.3353 13.2113 21.4838 15.1763C18.4531 17.2703 16.5158 21.8956 18.3976 26.2426C20.4526 30.9818 26.3273 32.3956 29.8238 30.9856C31.5938 30.2708 32.4098 32.6603 30.5701 33.4426C27.7973 34.6253 20.0701 34.5046 16.4611 28.2548C14.0228 24.0331 14.1518 16.6066 20.6213 12.7591C25.5661 9.81382 32.0903 10.6298 36.0233 14.7361C40.1333 19.0328 39.8956 27.0721 35.8831 30.1973C34.0658 31.6186 31.3673 30.2363 31.3868 28.1656L31.3666 27.4906C30.1013 28.7438 28.4176 29.4788 26.5853 29.4788C22.9606 29.4788 19.7693 26.2868 19.7693 22.6658C19.7693 19.0058 22.9606 15.7801 26.5853 15.7801V15.7786ZM31.1446 22.4041C31.0073 19.7513 29.0386 18.1538 26.6596 18.1538H26.5696C23.8276 18.1538 22.3043 20.3138 22.3043 22.7626C22.3043 25.5083 24.1441 27.2423 26.5583 27.2423C29.2531 27.2423 31.0223 25.2706 31.1513 22.9381L31.1446 22.4041Z" fill="#251D2A"/>
</g>
<defs>
<filter id="filter0_d_444_3500" x="0" y="0" width="53" height="53" filterUnits="userSpaceOnUse" colorInterpolationFilters="sRGB">
<feFlood floodOpacity="0" result="BackgroundImageFix"/>
<feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
<feOffset dy="4"/>
<feGaussianBlur stdDeviation="2"/>
<feComposite in2="hardAlpha" operator="out"/>
<feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0"/>
<feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_444_3500"/>
<feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_444_3500" result="shape"/>
</filter>
<clipPath id="clip0_444_3500">
<rect width="24" height="24" fill="white" transform="translate(15 11)"/>
</clipPath>
</defs>
</svg>

  );
};

export default MailIcon;
