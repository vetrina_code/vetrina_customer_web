const MessageIcon = () => {
  return (
    <svg
      width="53"
      height="53"
      viewBox="0 0 53 53"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g filter="url(#filter0_d_444_3499)">
        <path
          d="M4 22.5C4 10.0736 14.0736 0 26.5 0C38.9264 0 49 10.0736 49 22.5C49 34.9264 38.9264 45 26.5 45C14.0736 45 4 34.9264 4 22.5Z"
          fill="white"
        />
      </g>
      <path
        d="M29 30C32.771 30 34.657 30 35.828 28.828C37 27.657 37 25.771 37 22C37 18.229 37 16.343 35.828 15.172C34.657 14 32.771 14 29 14H25C21.229 14 19.343 14 18.172 15.172C17 16.343 17 18.229 17 22C17 25.771 17 27.657 18.172 28.828C18.825 29.482 19.7 29.771 21 29.898"
        stroke="#251D2A"
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M27 22V22.01"
        stroke="#251D2A"
        strokeWidth="2"
        strokeLinecap="round"
      />
      <path
        d="M23 22V22.01"
        stroke="#251D2A"
        strokeWidth="2"
        strokeLinecap="round"
      />
      <path
        d="M31 22V22.01"
        stroke="#251D2A"
        strokeWidth="2"
        strokeLinecap="round"
      />
      <path
        d="M29 30C27.764 30 26.402 30.5 25.159 31.145C23.161 32.182 22.162 32.701 21.67 32.37C21.178 32.04 21.271 31.015 21.458 28.966L21.5 28.5"
        stroke="#251D2A"
        strokeWidth="2"
        strokeLinecap="round"
      />
      <defs>
        <filter
          id="filter0_d_444_3499"
          x="0"
          y="0"
          width="53"
          height="53"
          filterUnits="userSpaceOnUse"
          colorInterpolationFilters="sRGB"
        >
          <feFlood floodOpacity="0" result="BackgroundImageFix" />
          <feColorMatrix
            in="SourceAlpha"
            type="matrix"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
            result="hardAlpha"
          />
          <feOffset dy="4" />
          <feGaussianBlur stdDeviation="2" />
          <feComposite in2="hardAlpha" operator="out" />
          <feColorMatrix
            type="matrix"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0"
          />
          <feBlend
            mode="normal"
            in2="BackgroundImageFix"
            result="effect1_dropShadow_444_3499"
          />
          <feBlend
            mode="normal"
            in="SourceGraphic"
            in2="effect1_dropShadow_444_3499"
            result="shape"
          />
        </filter>
      </defs>
    </svg>
  );
};

export default MessageIcon;
