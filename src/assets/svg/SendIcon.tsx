import React from "react";

export interface SvgProps {
  height: number;
  width: number;
}
const ExpandIcon = () => {
  return (
    <svg
      width="18"
      height="18"
      viewBox="0 0 18 18"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M7.434 8.99997H3L1.51725 3.10123C1.50775 3.06694 1.50197 3.03174 1.5 2.99622C1.4835 2.45547 2.079 2.08048 2.595 2.32798L16.5 8.99997L2.595 15.672C2.085 15.9172 1.497 15.5527 1.5 15.0217C1.50152 14.9743 1.50985 14.9273 1.52475 14.8822L2.625 11.25"
        stroke="#5C0E90"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

export default ExpandIcon;
