const LogoIcon = () => {
  return (
    <svg
      width="348"
      height="348"
      viewBox="0 0 348 348"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M124.029 235.371H187.8L144.281 112.86H80.504L124.029 235.371Z"
        fill="white"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M197.945 273.528H134.209L203.76 74.704H267.496L197.945 273.528Z"
        fill="#5C0E90"
      />
    </svg>
  );
};

export default LogoIcon;
