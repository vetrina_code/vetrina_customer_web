import React from "react";
import { IntlProvider } from "react-intl";
// Core Components
import AppRouter from "./router";
import { SupportModal } from "./components/Modals";

// Functions & Hooks
import { useAppDispatch, useAppSelector } from "./app/store";
import { setUser } from "./features/Authentication";
import { resetAppState, selectAppState } from "./features/App";

// Design & Icons
import { ChatIcon } from "./assets/svg";
import { ModalToolTip } from "./components/Modals/components";
import { Alert, Backdrop, CircularProgress, Grid, IconButton, Snackbar } from "@mui/material";
import "./App.css";



export default function App() {
  const dispatch = useAppDispatch();
  const { error, isError, isLoading, isSuccess, success } = useAppSelector(selectAppState);
  const handleSnackbarClose = () => {
    dispatch(resetAppState());
  };
  const [modalOpen, setModalOpen] = React.useState(false);
  const handleSupportModal = () => {
    setModalOpen(!modalOpen);
  };
  return (
    
    <IntlProvider locale="en" defaultLocale="en">
      <Backdrop
        sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={isLoading || false}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
      <Snackbar
        open={isError || isSuccess || false}
        onClose={() => handleSnackbarClose()}
        autoHideDuration={6000}
        anchorOrigin={{ vertical: "top", horizontal: "right" }}
      >
        <Alert
          severity={
             isSuccess? "success" : "error"
          }
          sx={{ width: "100%" }}
        >
          {(isSuccess ? success: error) || ''}
        </Alert>
      </Snackbar>
      <Grid
        container
        direction="row"
        justifyContent="flex-end"
        alignItems="flex-end"
        sx={{
          background: "transparent",
          position: "fixed",
          top: "80vh",
        }}
      >
        <Grid item>
          <ModalToolTip title={<SupportModal/>}
            open={modalOpen}
            onClose={handleSupportModal}
            disableTouchListener
            disableFocusListener
            disableHoverListener
            placement="top"
          >
            <IconButton onClick={handleSupportModal}>
              <ChatIcon />
            </IconButton>
          </ModalToolTip>
        </Grid>
      </Grid>
      <AppRouter />
    </IntlProvider>
  );
}
