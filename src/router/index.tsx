import React, { Suspense } from "react";
import { Routes, Route, useLocation } from "react-router-dom";
import Footer from "../components/Gen/Footer";
import Header from "../components/Gen/NavBar";

import { routes, protected_routes } from "./routes";

function Router() {
  const location = useLocation();
  const { pathname } = location;
  const isHome = pathname === "/";

  return (
    <React.Fragment>
      {pathname !== "/auth" && pathname !== "/reset-password" && (
        <Header />
      )}
      <Suspense fallback={<div>Loading...</div>}>
        <Routes>
          {routes.map((route, index) => {
            return (
              <Route
                key={index}
                path={route.path}
                element={<route.component />}
              />
            );
          })}
          {protected_routes.map((route, index) => {
            return (
              <Route
                key={index}
                path={route.path}
                element={<route.component />}
              />
            );
          })}
        </Routes>
      </Suspense>
      {pathname !== "/auth" && pathname !== "/reset-password" && <Footer />}
    </React.Fragment>
  );
}

export default Router;
