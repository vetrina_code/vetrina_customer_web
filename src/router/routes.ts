import React from "react";
// import LandingView from '../views/Home/Landing.view';
// import MarketplaceView from '../views/Home/MarketPlace.view';
// import AuthView from '../views/Auth/Auth.view';
// import ResetPasswordView from '../views/Auth/PasswordReset.view';

const LandingView = React.lazy(() => import('../views/Home/Landing.view'));
const MarketplaceView = React.lazy(() => import('../views/Home/MarketPlace.view'));
const AuthView = React.lazy(() => import('../views/Auth/Auth.view'));
const ResetPasswordView = React.lazy(() => import('../views/Auth/PasswordReset.view'));
const MerchantPage = React.lazy(() => import('../views/Merchant/Profile.view'));
const ProfilePage = React.lazy(() => import('../views/Identity/Profile.view'));

type RoutesRenderType =  {
  name: string,
  path: string,
  component: any,
}

export const protected_routes: Array<RoutesRenderType>= [
  {
    name: "Profile",
    path: '/profile',
    component: ProfilePage
  }
]
export const routes: Array<RoutesRenderType>= [
  {
    name: 'MarketPlace',
    path: '/marketplace',
    component: MarketplaceView
  },
  {
    name: 'MerchantPlace',
    path: '/merchant/:id',
    component: MerchantPage
  },
  {
    name: 'Home',
    path: '/',
    component: LandingView
  },
 {
    name: 'Auth',
    path: '/auth',
    component: AuthView,
  },
 {
    name: 'Auth',
    path: '/reset-password',
    component: ResetPasswordView,
  },
]